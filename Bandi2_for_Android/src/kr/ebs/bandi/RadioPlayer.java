package kr.ebs.bandi;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.AttributeSet;
import android.widget.VideoView;

public class RadioPlayer extends VideoView implements OnPreparedListener, OnCompletionListener, OnErrorListener
{
	private MediaPlayer mediaPlayer;
	public RadioPlayer(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public RadioPlayer(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onPrepared(MediaPlayer mp)
	{
		// TODO Auto-generated method stub
		mediaPlayer = mp;
	}
	
	public void mute()
	{
		mediaPlayer.setVolume(0, 0);
	}

	@Override
	public void onCompletion(MediaPlayer mp)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
