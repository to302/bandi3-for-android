package kr.ebs.bandi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;

@SuppressLint("NewApi")
public class TutorialDialog extends Dialog
{
	Context context;
	Activity activity;
	
	int[] tutorialImgRes = {R.drawable.tutorial_0, R.drawable.tutorial_1, R.drawable.tutorial_2, R.drawable.tutorial_3};
	ImageView tutorialImg;
	FrameLayout prevBtn, nextBtn, finishBtn, closeBtn;
	int index = 0;
	Preferences prefs;
	
	private DisplayMetrics dm;
	private int dpWidth, dpHeight;
	
	FrameLayout.LayoutParams fParams;
	
	private volatile static TutorialDialog single;
    public static TutorialDialog getInstance(Context context)
    {
        if (single == null) 
        {
            synchronized(TutorialDialog.class) 
            {
                if (single == null) 
                {
                    single = new TutorialDialog(context);
                }
            }
        }
        return single;
    }
	
	private TutorialDialog(Context context) 
	{
		super(context);
		this.context = context; 
		activity = (Activity)context;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tutorial_dialog);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.dimAmount = 0.5f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
		
		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		prefs = new Preferences(context);
		index = 0;
		viewSetting();
	}
	
	
	public void viewSetting()
	{
		tutorialImg = (ImageView)findViewById(R.id.imageView_tutorial);
		tutorialImg.setBackgroundResource(tutorialImgRes[index]);
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(150), getWidthDP(40), Gravity.BOTTOM);
		fParams.setMargins(getWidthDP(10), 0, 0, getWidthDP(45));
		prevBtn = (FrameLayout)findViewById(R.id.layout_tutorialPrev);
		prevBtn.setLayoutParams(fParams);
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(150), getWidthDP(40), Gravity.BOTTOM | Gravity.RIGHT);
		fParams.setMargins(0, 0, getWidthDP(10), getWidthDP(45));
		nextBtn = (FrameLayout)findViewById(R.id.layout_tutorialNext);
		nextBtn.setLayoutParams(fParams);
		
		fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(40), Gravity.BOTTOM);
		closeBtn = (FrameLayout)findViewById(R.id.layout_tutorialClose);
		closeBtn.setLayoutParams(fParams);
		
		prevBtn.setOnClickListener(clickListener);
		nextBtn.setOnClickListener(clickListener);
		closeBtn.setOnClickListener(clickListener);
	}
	
	
	
	android.view.View.OnClickListener clickListener = new android.view.View.OnClickListener() 
	{
		
		@Override
		public void onClick(View v) 
		{
			if(v.getId() == R.id.layout_tutorialPrev)
			{
				if (index == 0) { // 첫페이지에 '앞으로 띄우지 않기' 기능 추가. 2015-11-05
					prefs.putTutorialState(true);
					dismiss();
				} 
				else if(index > 0)
				{
					index--;
					
					tutorialImg.setBackgroundResource(tutorialImgRes[index]);
				}
			}
			else if(v.getId() == R.id.layout_tutorialNext)
			{
				if(index < 4)
				{
					if(index == 3)
					{
						prefs.putTutorialState(true);
						dismiss();
//						recycleView(tutorialImg);
					}
					else
					{
						index++;
						
						tutorialImg.setBackgroundResource(tutorialImgRes[index]);
					}
				}
			}
			else if(v.getId() == R.id.layout_tutorialClose)
			{
				dismiss();
				
			}
		}
	};
	
	public int getWidthDP(int dpValue)
	{
    	
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}
	
	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}
	
}
