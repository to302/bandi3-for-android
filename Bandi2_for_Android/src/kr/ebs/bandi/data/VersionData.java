package kr.ebs.bandi.data;



public class VersionData 
{
	
	//resultCd(결과 코드 000:정상, 111:오류) 
	//resultMsg(결과 메시지)
	String appName; //bandi
	String appVer;// 반디 버전
	String appUrl; //반디 앱 다운로드 경로(현재 PC만 데이터가 있음)
	String appMsg;// : 앱 다운로드시 노출할 메시지
	String appDsCd;
		
	private volatile static VersionData single;
    public static VersionData getInstance()
    {
        if (single == null) 
        {
            synchronized(VersionData.class) 
            {
                if (single == null) 
                {
                    single = new VersionData();
                }
            }
        }
        return single;
    }
	private VersionData(){}
	
	public void setAppName(String appName)
	{
		this.appName = appName;
	}
	
	public void setBandiVersion(String version)
	{
		this.appVer = version;
	}
	
	public void setAppURL(String url)
	{
		this.appUrl = url;
	}
	
	public void setAppMessage(String msg)
	{
		this.appMsg = msg;
	}
	
	public void setAppDsCd(String appDsCd)
	{
		this.appDsCd = appDsCd;
	}
	
	public String getAppName()
	{
		return appName;
	}
	
	public String getBandiVersion()
	{
		return appVer;
	}
	
	public String getAppURL()
	{
		return appUrl;
	}
	
	public String getMessage()
	{
		return appMsg;
	}
	
	public String getAppDsCd()
	{
		return appDsCd;
	}
	
}
