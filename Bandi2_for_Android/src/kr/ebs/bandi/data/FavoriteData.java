package kr.ebs.bandi.data;


//fbmkSno: 23,
//userId: null,
//courseId: "10009937",
//deltYn: null,
//siteDsCd: null,
//appDsCd: null,
//crtnDtm: null,
//crtnUserId: null,
//lstChngDtm: null,
//lstChngUserId: null


public class FavoriteData 
{
	
	int fbmkSno;
	String courseId;
	String title;
	String linkUrl;
	
	public FavoriteData(){}
	
	public void setFbmkSno(int num)
	{
		fbmkSno = num;
	}
	
	public void setProgramID(String id)
	{
		courseId = id;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void setLinkUrl(String url)
	{
		linkUrl = url;
	}
	
	public int getFbmkSno()
	{
		return fbmkSno;
	}
	
	/**
	 * courseId 반환
	 * @return courseId
	 */
	public String getProgramID()
	{
		return courseId;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getLinkUrl()
	{
		return linkUrl;
	}
}
