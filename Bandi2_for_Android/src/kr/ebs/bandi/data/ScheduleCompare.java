package kr.ebs.bandi.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import kr.ebs.bandi.BandiApplication;
import kr.ebs.bandi.DateUtil;
import kr.ebs.bandi.TimeTable;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.util.Log;

public class ScheduleCompare
{
	static final String TAG = "ScheduleCompare";
	
	private JSONArray data = null;
	private OnCompareListener listener;
	String nowId, prevId;
	/**
	 * 사용되지 않는 변수 인듯...
	 */
	ArrayList<TimeData> timeData;
	
	private volatile static ScheduleCompare single;
    public static ScheduleCompare getInstance()
    {
        if (single == null) 
        {
            synchronized(ScheduleCompare.class) 
            {
                if (single == null) 
                {
                    single = new ScheduleCompare();
                }
            }
        }
        return single;
    }
	private ScheduleCompare(){}
	
	public void setTimeData(ArrayList<TimeData> data)
	{
		timeData = new ArrayList<TimeData>(data);
	}
	
	
	/**
	 * 호출되는 곳이 없다.
	 * @return
	 */
	public ArrayList<TimeData> getTimeData()
	{
		return timeData;
	}
	
	public JSONArray getData()
	{
		return data;
	}
	
	public void setData(JSONArray data)
	{
		this.data = data;
	}
	
	public void setListener(OnCompareListener listener)
	{
		this.listener = listener;
	}
	Timer timer;
	boolean isStart;
	
	/**
	 * MainActivity 에서 호출됨.
	 */
	public void compareStart()
	{
		isStart = true;
		timer = new Timer();
		timer.schedule(radioCompare, 0, 1000);
	}
	
	public void init()
	{
		isStart = true;
	}
	
	String nowOnAirId;
	public String getNowOnAirId()
	{
		return nowOnAirId;
	}
	
	
	TimerTask radioCompare = new TimerTask() 
	{
		
		@Override
		public void run() 
		{
//			Log.d(TAG, "radioCompare - TimeTask run()");
			boolean isScheduleOver = true;
			boolean isDataChange = false;
			int index = 0;
	        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd HH:mm", Locale.KOREA);
	        
			try 
			{
				if(data != null)
				{
					for(int i = 0; i < data.length(); i++)
					{
						Date start = null, end = null;
						if(i == data.length() - 1)
						{
							start = dateFormat.parse(data.getJSONObject(i).getString("onairDate") + " " + data.getJSONObject(i).getString("start"));
							end = dateFormat.parse(data.getJSONObject(i).getString("onairDate") + " " + "26:00");
						}
						else
						{
							start = dateFormat.parse(data.getJSONObject(i).getString("onairDate") + " " + data.getJSONObject(i).getString("start"));
							end = dateFormat.parse(data.getJSONObject(i).getString("onairDate") + " " + data.getJSONObject(i + 1).getString("start"));
						}
						
						
						
						Date now = dateFormat.parse(getDateForXmlRetrieving() + " " + getTimeForSchedule());
						if(now.getTime() - start.getTime() == 0)
						{
							Log.d("TEST", "time same == " + now.getTime() + " index == " + i);
							index = i;
							if(isStart == false)
							{
								nowId = data.getJSONObject(i).getString("title");
								nowOnAirId = data.getJSONObject(i).getString("id");
								if(nowId.equals(prevId) == false)
								{
									index = i;
									isDataChange = true;
								}
								
								prevId = nowId;
							}
							else
							{
								nowOnAirId = data.getJSONObject(i).getString("id");
								nowId = data.getJSONObject(i).getString("title");
								prevId = nowId;
								if(listener != null) listener.OnSchedule(data.getJSONObject(index).getString("id"), data.getJSONObject(index).getString("title"), data.getJSONObject(index).getString("onairDate"), data.getJSONObject(index).getString("start"));
							}
							isScheduleOver = false;
						}
						else
						{
							if(now.getTime() - start.getTime() > 0 && now.getTime() - end.getTime() < 0)
							{
								index = i;
								if(isStart == false)
								{
									nowId = data.getJSONObject(i).getString("title");
									nowOnAirId = data.getJSONObject(i).getString("id");
									if(nowId.equals(prevId) == false)
									{
										isDataChange = true;
									}
									
									prevId = nowId;
								}
								else
								{
									nowId = data.getJSONObject(i).getString("title");
									nowOnAirId = data.getJSONObject(i).getString("id");
									if(listener != null) listener.OnSchedule(data.getJSONObject(index).getString("id"), data.getJSONObject(index).getString("title"), data.getJSONObject(index).getString("onairDate"), data.getJSONObject(index).getString("start"));
									prevId = nowId;
								}
								isScheduleOver = false;
							}
						}
					}
					
					if(isScheduleOver)
					{
						if(listener != null) listener.OnScheduleFinish();
					}
					else
					{
						if(isDataChange)
						{
							if(listener != null) listener.OnScheduleChange(data.getJSONObject(index).getString("id"), data.getJSONObject(index).getString("title"), data.getJSONObject(index).getString("onairDate"), data.getJSONObject(index).getString("start"));
						}
						else
						{
							
						}
					}
					
					isStart = false;
				}
				else
				{
					if(listener != null) listener.OnScheduleError();
				}
			}
			catch (ParseException e) 
			{
				if(listener != null) listener.OnScheduleError();
				e.printStackTrace();
			}
			catch (JSONException e)
			{
				if(listener != null) listener.OnScheduleError();
				e.printStackTrace();
			}
		}
	};
	
	/**
     * 편성표용 날짜 변경 보정 시간. (익일 2시에 편성표 호출날짜 변경)
     */
    public static final int HOUR_ADJUSTMENT = -2;
	
	/**
     * xml file 을 받아오기 위한 날짜 string 을 반환 (예.20120312) <br />
     * 라디오 방송 종료시간을 고려해서 2시간 늦은 날짜를 반영. <br />
     * (2월 2일 01시 29분 일 경우 -> 2월 1일 날짜로 반환)
     * @return
     */
    public String getDateForXmlRetrieving() 
    {
        Calendar cal = DateUtil.getKoreanCalendar();
        cal.add(Calendar.HOUR_OF_DAY, HOUR_ADJUSTMENT); // 라디오 방송 종료 시간 (익일 새벽 2시)
      
        String date = DateUtil.get8DigitsDateString(cal);
        return date;
    }
    
    /**
     * GMT+9 시간(한국시간)으로 보정된 시간 반환.
     *
     * @since 2012. 3. 3.
     * @return
     */
    public static Calendar getKoreanCalendar() 
    {
        return DateUtil.getKoreanCalendar();
    }
    
    private String set2Digits(int dateNum) 
    {
        return String.format("%02d", dateNum);
    	
    }
    
        
    /**
     * 한국시간 기준(GMT+9 설정) 으로 시간정보 반환 (24시간).  예)14:10 <br />
     * 편성표 정보를 비교하기 위해.. 익일 01 시 -> 25시로 반환..
     * @return
     */
    public String getTimeForSchedule() 
    {
        String time = "";
        int adjustingHour = 0;
        String currDate = DateUtil.get8DigitsDateString(DateUtil.getKoreanCalendar());
        
        // 현재일과 편성표 xml 파일 추출용 날짜가 다르면..
        if (! currDate.equals(getDateForXmlRetrieving())) 
        {
            adjustingHour = 24;
        }
        Calendar cal = DateUtil.getKoreanCalendar();

        time = set2Digits(cal.get(Calendar.HOUR_OF_DAY) + adjustingHour) + ":" + set2Digits(cal.get(Calendar.MINUTE));
        return time;

    }
    
    public interface OnCompareListener
    {
    	public void OnScheduleChange(String id, String title, String onairDate, String onairTime);
    	public void OnSchedule(String id, String title, String onairDate, String onairTime);
    	public void OnScheduleFinish();
    	public void OnScheduleError();
    }
}
