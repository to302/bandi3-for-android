package kr.ebs.bandi.data;


//bookId: 6,
//bookNm: "테스트 도서정보_03 (모바일 URL 등록)",
//startDtm: "2014-12-21",
//endDtm: "2015-07-31",
//bookWritr: null,
//bookCntn: null,
//mobLink: "http://m.ebs.co.kr",
//thmnlFilePathNm: null,
//thmnlFileLogcNm: null,
//thmnlFilePhscNm: null,
//thmnlFilePathNm2: null,
//thmnlFileLogcNm2: null,
//thmnlFilePhscNm2: null,
//imgUrl: "http://static.ebs.co.kr/images/bhp/bandi/images/2014/12/21/18/8/37/932f5e71-858f-4d91-b81b-81e07a3f40eb.jpg",
//mobImgUrl: "http://static.ebs.co.kr/images/bhp/bandi/images/2014/12/21/18/8/37/64eb07c5-8c56-428a-9a87-169b603eacf4.jpg",
//shwSeq: 1,
//hitFig: 0,
//mobHitFig: 0,
//appDsCd: null,
//courseId: "zzz",
//crtnDtm: "2014-12-21",
//crtnUserId: null,
//lstChngDtm: null,
//lstChngUserId: null,
//listSeqNo: null

public class BookInfoData
{
	int bookId;
	String bookNm;
	String startDtm;
	String endDtm;
	String bookWritr;
	String bookCntn;
	String mobLink;
	/*String thmnlFilePathNm: null,
	String thmnlFileLogcNm: null,
	String thmnlFilePhscNm: null,
	String thmnlFilePathNm2: null,
	String thmnlFileLogcNm2: null,
	String thmnlFilePhscNm2: null,*/
	String mobImgUrl;
	String crtnDtm;
	
//	appDsCd: null,
//	courseId: "zzz",
//	crtnDtm: "2014-12-21",
//	crtnUserId: null,
//	lstChngDtm: null,
//	lstChngUserId: null,
//	listSeqNo: null
	
	/*private volatile static BookInfoData single;
    public static BookInfoData getInstance()
    {
        if (single == null) 
        {
            synchronized(BookInfoData.class) 
            {
                if (single == null) 
                {
                    single = new BookInfoData();
                }
            }
        }
        return single;
    }*/
	public BookInfoData(){}
	
	public void setBookID(int id) { bookId = id; }
	public void setBookName(String name) { bookNm = name; }
	public void setBookDescription(String text) { bookCntn = text; }
	public void setStartDay(String day) { startDtm = day; }
	public void setEndDay(String day) { endDtm = day; }
	public void setLinkUrl(String url) { mobLink = url; }
	public void setImageUrl(String url) { mobImgUrl = url; }
	public void setRegisterDay(String day) { crtnDtm = day; }
	
	public int getBookID() { return bookId; }
	public String getBookName() { return bookNm; }
	public String getBookDescription() { return bookCntn; }
	public String getStartDay() { return startDtm; }
	public String getEndDay() { return endDtm; }
	public String getLinkUrl() { return mobLink; }
	public String getImageUrl() { return mobImgUrl; };
	public String getRegisterDay() { return crtnDtm; }
}
