/**
 * 반디 오디오 스트리밍 서비스.
 */
package kr.ebs.bandi;


import java.net.MalformedURLException;
import java.net.URL;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

/**
 * @since 2012. 3. 26.
 */
public class AudioService extends Service 
{

    final static String TAG = "AudioService";

    
//    static final String WIFI_LOCK_TAG = "wifi_lock_bandi_audio";

//    private int startId;
    private String AUDIO_URL = ""; // 기본 URL, 아래에서 바뀜. (setStreamingUrl)

    static public enum Channel {IRADIO, FM};
    private Channel currentChannel ;
    
    private String CDNETWORKS_URL = ""; // ok 2014-07-28 변경.
    private String CDNETWORKS_URL_HLS = ""; // 2014-07-28 변경
                                               
    private MediaPlayer mediaPlayer = null;
//    private WifiLock wifiLock = null;
    private AudioManager audioManager;
    private PowerManager.WakeLock wakeLock = null;

    // 재생 버튼
    private FrameLayout playController = null;
    private ProgressBar loadingAudio = null;

    // 사용자의 재생, 멈춤 기능 선택 행동 저장
    private boolean isPlayingPhase = false;

    // 오디오 전용 스트림 사용 (hls 의 경우 내용없는 video 스트림이 옴)
    private boolean useAudioStream = true;

    BandiApplication bandiApp;
    
    private SeekBar volume;
    
    private Preferences prefs;
    
    //** for doze mode ** 
    // 2017-04-18 : 불필요한 것 같아서.. 일단 주석 처리.. 나중에 지울것.
    private BroadcastReceiver receiver = new IdleReceiver(); 
    
    public static class IdleReceiver extends IdleChangeReceiver {
        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onReceive(Context context, Intent intent) {
        	Log.d(AudioService.TAG, "IdleReceiver.onReceive is called.");
            PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
            if (pm.isDeviceIdleMode()){
                launchMaskActivtiy(context);
            }
        }
    }
    //** for doze mode ****/
    
    // audio focus control
    public AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() 
    {
        public void onAudioFocusChange(int focusChange) 
        {
        	
        	Log.d(TAG, "audioFocusChangeListener == " + focusChange);
        	
            switch(focusChange) 
            {
            
            	
                case AudioManager.AUDIOFOCUS_GAIN:
                    if (! isPlaying()) initPlayer();
                    else mediaPlayer.setVolume(1.0f, 1.0f);
                    Log.d(TAG, ">> gain" );
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    Log.d(TAG, ">> stop" );
                    stopPlayer();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    mediaPlayer.setVolume(0.01f, 0.01f);
                    break;
                
            }
        }
    };


    private final IBinder serviceBinder = new ServiceBinder();
    public class ServiceBinder extends Binder 
    {
        AudioService getService() 
        {
            return AudioService.this;
        }
    }

    
    
    /**
     * 스트리밍 URL 셋팅
     * @throws MalformedURLException 
     * @since 2012. 6. 27.
     */
    private void setStreamingUrl()
    {
    	
        String tmp = this.AUDIO_URL;
        try 
        {       
        	URL url;
        	String urlStr;
        	
        	if (prefs.getHLS())
        	{
        		urlStr = this.CDNETWORKS_URL_HLS;
        		
        		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP 
        				&& Channel.IRADIO.equals(this.currentChannel)) 
        		{
        			urlStr = this.CDNETWORKS_URL;
        		}
        	}
        	else 
        	{
        		urlStr = this.CDNETWORKS_URL;
        	}
        	Log.d(TAG, ">> urlStr : "+urlStr);
        	
    		try 
    		{
    			url = new URL(urlStr);
    			String host = url.getHost();
    			String ip = Url.getHostIPv4(host);
//    			String ip = Url.getServerIP(host);
    			urlStr = urlStr.replace(host, ip);
    			
    		} 
    		catch (MalformedURLException e1) 
    		{
    			e1.printStackTrace();
    		}
    		finally 
    		{
    			this.AUDIO_URL = urlStr;
    		}
    		
        	
//            if ("4.4.3".equals(Build.VERSION.RELEASE) || "4.4.4".equals(Build.VERSION.RELEASE)) 
//            {
//            	// 4.4.3 과 4.4.4 에서 m3u8 재생 버그 (2014-07-02)
//            	// 참고 - http://stackoverflow.com/questions/24501827/when-using-the-mediaplayer-api-the-audio-stream-stops-after-a-few-seconds-of-pla
//            	this.AUDIO_URL = this.CDNETWORKS_URL;
//            
//            }
//            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)  
//            { 
//            	// android 4.1 이상 버전에서는 hls 사용.
//            	URL url;
//            	String urlHls = this.CDNETWORKS_URL_HLS;
//        		try 
//        		{
//        			url = new URL(CDNETWORKS_URL_HLS);
//        			String host = url.getHost();
//        			String ip = Url.getServerIP(host);
//        			urlHls = CDNETWORKS_URL_HLS.replace(host, ip);
//        		} 
//        		catch (MalformedURLException e1) 
//        		{
//        			e1.printStackTrace();
//        		}
//            	
//            	this.AUDIO_URL = urlHls;
//            	this.useAudioStream = false;
//            }
//            else if(Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP)
//            {
//            	this.AUDIO_URL = this.CDNETWORKS_URL;
//            }
//            else 
//            {
//            	this.AUDIO_URL = this.CDNETWORKS_URL;
//            }
        } 
        catch (Exception e) 
        {
            this.AUDIO_URL = tmp;
        }
        
    }

    /**
     * 현재 streaming url 반환.
     * @since 2012. 7. 18.
     * @return
     */
    protected String getStreamingUrl() 
    {
    	
    	if (this.AUDIO_URL == null || "".equals(this.AUDIO_URL)) this.setStreamingUrl();

        return this.AUDIO_URL;
    }


    @Override
    public void onCreate() 
    {
    	bandiApp = (BandiApplication)getApplication();

    	prefs = new Preferences(this);
    	
    	if(prefs.getIsStartFMRadio())
    	{
    		CDNETWORKS_URL = bandiApp.getStreamUrlAndroid();
            CDNETWORKS_URL_HLS = bandiApp.getStreamUrlAndroid2();
            currentChannel = Channel.FM;
    	}
    	else
    	{
    		CDNETWORKS_URL = bandiApp.getStreamUrlIradioAndroid();
            CDNETWORKS_URL_HLS = bandiApp.getStreamUrlIradioAndroid2();
            currentChannel = Channel.IRADIO;
    	}
    	
        setStreamingUrl();
    }

    @Override
    public void onDestroy() 
    {
    	Log.d(TAG, "onDestroy");
    	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            unregisterReceiver(receiver); // 2017-04-18 : 불필요한 것 같아서.. 일단 주석 처리.. 나중에 지울것.
        }
        stopPlayer();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) 
    {
    	Log.d(TAG, "onStartCommand()");
//        this.startId = startId;
    	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            registerReceiver(receiver, IdleReceiver.Filter); // 2017-04-18 : 불필요한 것 같아서.. 일단 주석 처리.. 나중에 지울것.
        }
        return START_STICKY;
    }

    /* (non-Javadoc)
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) 
    {
        Log.d(TAG, "onBind()");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            registerReceiver(receiver, IdleReceiver.Filter);// 2017-04-18 : 불필요한 것 같아서.. 일단 주석 처리.. 나중에 지울것.
            Log.d(TAG, "register idleReceiver.");
        }
        return serviceBinder;
    }

    @Override
    public void onRebind(Intent intent) 
    {
        Log.d(TAG, "onRebind()");
    }

    public boolean onUnbind(Intent intent) 
    {
        return true;
    }


    public void initPlayer(FrameLayout view, ProgressBar loading) 
    {
        this.playController = view;
        this.loadingAudio = loading;
        playController.setSelected(true);
        loading.setVisibility(View.VISIBLE);
        
        initPlayer();
    }

    public void setSeekBar(SeekBar seek)
    {
    	volume = seek;
    }
    
    long startTime, endTime;
    public void initPlayer() 
    {
        // get audio focus
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) 
        {
            Toast.makeText(getApplicationContext(), "Error - 오디오 기능을 수행하지 못했습니다.\n다시 재생버튼을 눌러주세요.", Toast.LENGTH_SHORT).show();
        }

        if (wakeLock == null) 
		{
			PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wakelock");
		    wakeLock.acquire();
		}
        
        mediaPlayer = new MediaPlayer();
        if (this.useAudioStream)
        {
        	mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        try 
        {
        	Log.d(TAG, ">> AUDIO_URL : " + AUDIO_URL);
            mediaPlayer.setDataSource(AUDIO_URL);
            mediaPlayer.setOnPreparedListener(new OnPreparedListener() 
            {
                public void onPrepared(MediaPlayer mp) 
                {
                	
                	endTime = System.currentTimeMillis();
                	Log.d(TAG, "player prepare end time == " + endTime);
                	
                	Log.d(TAG, "time compare == " + ((endTime - startTime) / 1000.0f));
//                	Toast.makeText(AudioService.this, "prepare time = " + ((endTime - startTime) / 1000.0f) + "초", Toast.LENGTH_LONG).show();
                	mediaPlayer.start();
                	
//                	main.audio.setStreamVolume(AudioManager.STREAM_MUSIC, vol.getProgress(), AudioManager.MODE_NORMAL);
                    if (loadingAudio != null) loadingAudio.setVisibility(View.INVISIBLE);
                    if (playController != null)
                    {
                    	Log.d(TAG, "initPlayer");
                    	playController.setSelected(false);
                    }
                }
            });

            mediaPlayer.prepareAsync();
            startTime = System.currentTimeMillis();
            Log.d(TAG, "player prepare start time == " + startTime);
            mediaPlayer.setOnErrorListener(new OnErrorListener() 
            {
                public boolean onError(MediaPlayer mp, int what, int extra) 
                {
                    if (mediaPlayer != null) 
                    {
//                    	mediaPlayer.release();
                    	mediaPlayer.reset();
                    }
                    Log.e(TAG, "what:"+ what + "; extra:" + extra);
                    return true;
                }
                
            });

        } 
        catch(Exception e) 
        {
            // pass
        	e.printStackTrace();
        	Log.d(TAG, "error - ");
        }

        // network
//        wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, WIFI_LOCK_TAG);
//        wifiLock.acquire();

        this.isPlayingPhase = true;
    }

    /** 재생여부 확인 */
    public boolean isPlaying()
    {
        return (mediaPlayer != null && mediaPlayer instanceof MediaPlayer && mediaPlayer.isPlaying());

    }

    /**
     * 사용자의 최종 액션 (재생, 멈춤) 상태를 가져옴.
     */
    public boolean wasPlayingPhase()
    {
    	return this.isPlayingPhase;
    }

    public void stopPlayer(FrameLayout view, ProgressBar pBar)
    {
    	
    	pBar.setVisibility(View.INVISIBLE);
        view.setSelected(false); // set player controller image
        stopPlayer();
    }

    /** stop player & set null <br/>
     * wifi_lock release <br/>
     * abandon audio focus  <br/>
     */
    public void stopPlayer() 
    {
    	
    	Log.d("TEST", "stop player");
    	
        if (mediaPlayer != null ) 
        {
            if (mediaPlayer.isPlaying()) mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        
        if (wakeLock != null) 
		{
		    wakeLock.release();
		    wakeLock = null;
		}

//        if (wifiLock != null) 
//        {
//            if (wifiLock.isHeld()) wifiLock.release();
//        }
        
        // 재생버튼(삼각형)으로 보이기
        if (playController != null) playController.setSelected(true);
        
        audioManager.abandonAudioFocus(audioFocusChangeListener);

        this.isPlayingPhase = false;
        Log.d(TAG, "Done AudioService.stopPlayer()");
    }



    public static void start(Context context) 
    {
        Intent i = new Intent(context, AudioService.class);
        context.startService(i);
    }

    public static void stop(Context context)
    {
        Intent i = new Intent(context, AudioService.class);
        context.stopService(i);
    }
    
    public void switchUrl(String url, String url1)
    {
    	
    	CDNETWORKS_URL = url;
        CDNETWORKS_URL_HLS = url1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)  
        { 
        	useAudioStream = false;
        }
    	setStreamingUrl();
    }
    
    
    public void switchUrl(Channel channel, String url, String url1) {
    	this.currentChannel = channel;
    	this.switchUrl(url, url1);
    }
}
