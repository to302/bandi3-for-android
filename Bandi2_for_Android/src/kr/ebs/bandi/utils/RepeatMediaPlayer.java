package kr.ebs.bandi.utils;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.Log;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class RepeatMediaPlayer implements Runnable
{

	MediaPlayer mp;
	MediaController mediaController;
	String path;
	SeekBar bar;
	
	public RepeatMediaPlayer()
	{

		mp=new MediaPlayer();
        
    }

	@Override
	public void run() { //implements Runnable 로 부터 상속받았기에
		int current=0;
		while(mp!=null){
			try {
				Thread.sleep(1000);
				current=mp.getCurrentPosition();
				if(mp.isPlaying()){
					bar.setProgress(current);
     
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}  
	}
	OnSeekBarChangeListener onseek=new OnSeekBarChangeListener() {
  
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub   
		}
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub   
		}  
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			mp.seekTo(progress);
			bar.setProgress(progress);
//			int m=progress/60000;
//			int s=(progress%60000)/1000;
//			t1.setText(m+":"+s);
		}
	};
 
	public void stop()
	{
		
		try {
			mp.stop();
			mp.prepare();
			mp.seekTo(0);
			bar.setProgress(0);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		mp.pause();
	}
	
//	@Override
//	public void on_click(View v) { //implements on_clickListener로 부터 상속받았기에
//		if(v.getId()==R.id.play){
//			if(mp.isPlaying()){ // 음악이 플레이중이면
//				mp.pause();
//				play.setText("PLAY");
//			}else{
//				mp.start();
//				play.setText("PAUSE");
//			}
//		}else if(v.getId()==R.id.stop){
//			try {
//				mp.stop();
//				play.setText("PLAY");
//				mp.prepare(); // 미디어 플레이어의 라이프싸이클에 나옴, 플레이하기전에는 준비시켜야함
//				mp.seekTo(0);// 플레이어를 멈추면 seekTo : 위치를 맨앞으로 옮겨라
//				seekbar.setProgress(0); // 프로그래스바 위치도 맨앞으로 옮김
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	}
//		
//	}
	
	public void setView(SeekBar bar)
	{
		this.bar = bar;
	}
	
	public void setPath(String path)
	{
		this.path = path;
	}
	
	public void startPlay()
	{
		try {
			Log.d("TEST", "path === " + path);
        	mp.setDataSource(path); // setDataSource -> 경로 설정
        	mp.prepare(); // 플레이하기위해서 준비단계

        	//mpbar=(ProgressBar) findViewById(R.id.pgbar);
//        	seekbar=(SeekBar) findViewById(R.id.seekbar);
//        	seekvol=(SeekBar) findViewById(R.id.seekvolumn);// 볼륨표시를 위해
//        	audio=(AudioManager) getSystemService(AUDIO_SERVICE);
   
        	//쓰레드 생성
        	new Thread(RepeatMediaPlayer.this).start();// 자기자신을 start 시켜라
        	bar.setVisibility(ProgressBar.VISIBLE); // ProgressBar 를 보여달라
        	bar.setProgress(0);// ProgressBar 의 처음위치는 0
        	bar.setMax(mp.getDuration()); //mp.getDuration() -> 음악파일의 전체길이
//        	int kk=bar.getMax(); // 전체길이
//        	int m=kk/60000;
//        	int s=(kk%60000)/1000;
//        	t2.setText(m+":"+s);
   
//        	rw.seton_clickListener(this);
//        	play.seton_clickListener(this);
//        	stop.seton_clickListener(this);
//        	ff.seton_clickListener(this);
        	bar.setOnSeekBarChangeListener(onseek); // 진행시간

        } catch (Exception e) {
        	e.printStackTrace();
        }
        mp.setOnCompletionListener(new OnCompletionListener(){
        	@Override
        	public void onCompletion(MediaPlayer mp) {
        		bar.setProgress(0);
        	}   
        });
	}
	

}
