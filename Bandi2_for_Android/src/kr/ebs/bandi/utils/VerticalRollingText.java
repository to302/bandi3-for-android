package kr.ebs.bandi.utils;

import java.util.ArrayList;

import kr.ebs.bandi.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class VerticalRollingText extends TextView
{
	int count;
	Animation appearAni;
	Animation disappearAni;
	Animation appearStandby;
	Animation disappearStandby;
	ArrayList<String> text = new ArrayList<String>();
	public VerticalRollingText(Context context)
	{
		super(context);
	}
	
	public VerticalRollingText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public void rollingStart()
	{
		count = 0;
		appearAni = AnimationUtils.loadAnimation(getContext(), R.anim.appear);
		disappearAni = AnimationUtils.loadAnimation(getContext(), R.anim.disappear);
		appearStandby = AnimationUtils.loadAnimation(getContext(), R.anim.standby_appear);
		disappearStandby = AnimationUtils.loadAnimation(getContext(), R.anim.standby_disappear);
		
		appearAni.setAnimationListener(aniListener);
		disappearAni.setAnimationListener(aniListener);
		appearStandby.setAnimationListener(aniListener);
		disappearStandby.setAnimationListener(aniListener);
		
		setText(text.get(count));
		this.startAnimation(appearAni);
	}
	
	public void setRollingText(ArrayList<String> text)
	{
		this.text = new ArrayList<String>(text);
		
	}
	
	AnimationListener aniListener = new AnimationListener()
	{
		
		@Override
		public void onAnimationStart(Animation animation){}
		@Override
		public void onAnimationRepeat(Animation animation){}		
		@Override
		public void onAnimationEnd(Animation animation)
		{
			if(animation == appearAni)
			{
				startAnimation(appearStandby);
			}
			else if(animation == appearStandby)
			{
				startAnimation(disappearAni);
			}
			else if(animation == disappearAni)
			{
				startAnimation(disappearStandby);
			}
			else if(animation == disappearStandby)
			{
				if(count < text.size() - 1)
				{
					count++;
				}
				else
				{
					count = 0;
				}
				
//				if(text.get(count).equals("null") || text.get(count) == null)
//				{
//					setText("");
//				}
//				else
//				{
//					setText(text.get(count));
//				}
				
				setText(text.get(count));
				startAnimation(appearAni);
			}
		}
	};
}
