package kr.ebs.bandi;

import java.util.Map;

import android.app.Activity;
import android.content.Context;

public class APIWork {
	static final String TAG = "APIWork";
	
	private Context context;
	private Activity activity;
	
	public APIWork(Context context)
	{
		this.context = context;
		this.activity = (Activity)context;
	}
	
	public void doThreadJob(String url, Map<String, ?> params, APICallback callback)
	{
		workThread = new WorkThread(url, params, callback);
		workThread.start();
	}
	
	private WorkThread workThread;
	private class WorkThread extends Thread
	{
		private String url;
		private Map<String, ?> params;
		private APICallback callback;
		
		public WorkThread(String url, Map<String, ?> params, final APICallback callback)
		{
			this.url = url;
			this.params = params;
			this.callback = callback;
		}
		@Override
		public void run()
		{
			final String responseStr = Url.postHttpRequest(url, params, "utf-8");
			activity.runOnUiThread(new Runnable(){
				@Override
				public void run() {
					callback.callback(responseStr);
				}
			});
		}
	}
	
	interface APICallback
	{
		public void callback(String responseStr);
	}
	
}
