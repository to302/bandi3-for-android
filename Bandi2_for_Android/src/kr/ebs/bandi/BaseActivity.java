package kr.ebs.bandi;

import java.util.Calendar;

import kr.ebs.bandi.LoginForm.OnLoginFailureCb;

import com.facebook.Session;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;



public class BaseActivity extends Activity 
{
	static final String TAG = "BaseActivity";
	
	protected SlidingMenu baseMenu;
	BandiApplication bandiApp;
	OnAir air;
	public DisplayMetrics dm;
	public int dpWidth, dpHeight;
	
	public LinearLayout.LayoutParams lParams;
	public FrameLayout.LayoutParams fParams;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		bandiApp = (BandiApplication)getApplication();
		
		LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        RelativeLayout rootLayout = (RelativeLayout) inflater.inflate( R.layout.activity_main, null );
        
        /********
        RelativeLayout swapZone = (RelativeLayout) rootLayout.findViewById(R.id.menu_swap_zone);
        swapZone.removeAllViews();
        View view = (View) getLayoutInflater().inflate(R.layout.activity_on_air, swapZone); // �ʱⰪ on_air
        *********/
        
		setContentView( rootLayout );
		
		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
		
		setSlidingMenu(this); // 슬라이딩 메뉴
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(45));
		FrameLayout menuTop = (FrameLayout)findViewById(R.id.layout_menuTop);
		menuTop.setLayoutParams(lParams);
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(69), getWidthDP(22), Gravity.CENTER);
		ImageView menuIcon = (ImageView)findViewById(R.id.imageView_menuIcon);
		menuIcon.setLayoutParams(fParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(25), getWidthDP(25));
		lParams.setMargins(getWidthDP(15), 0, 0, 0);
		
		ImageView onAirIcon = (ImageView)findViewById(R.id.imageView_on_air);
		ImageView scheduleIcon = (ImageView)findViewById(R.id.imageView_air_schedule);
		ImageView boardIcon = (ImageView)findViewById(R.id.imageView_bandi_board_menu);
		ImageView favoriteIcon = (ImageView)findViewById(R.id.imageView_favorite);
		ImageView notiIcon = (ImageView)findViewById(R.id.imageView_noti_and_event);
		ImageView familyIcon = (ImageView)findViewById(R.id.imageView_family_service);
		ImageView settingIcon = (ImageView)findViewById(R.id.imageView_setting);
		
		onAirIcon.setLayoutParams(lParams);
		scheduleIcon.setLayoutParams(lParams);
		boardIcon.setLayoutParams(lParams);
		favoriteIcon.setLayoutParams(lParams);
		notiIcon.setLayoutParams(lParams);
		familyIcon.setLayoutParams(lParams);
		settingIcon.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), 0, 0, 0);
		
		TextView onAirText = (TextView)findViewById(R.id.on_air);
		TextView scheduleText = (TextView)findViewById(R.id.air_schedule);
		TextView boardText = (TextView)findViewById(R.id.bandi_board_menu);
		TextView favoriteText = (TextView)findViewById(R.id.favorite);
		TextView notiText = (TextView)findViewById(R.id.noti_and_event);
		TextView familyText = (TextView)findViewById(R.id.family_service);
		TextView settingText = (TextView)findViewById(R.id.setting);
		
		onAirText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		scheduleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		boardText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		favoriteText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		notiText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		familyText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		settingText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		
		onAirText.setLayoutParams(lParams);
		scheduleText.setLayoutParams(lParams);
		boardText.setLayoutParams(lParams);
		favoriteText.setLayoutParams(lParams);
		notiText.setLayoutParams(lParams);
		familyText.setLayoutParams(lParams);
		settingText.setLayoutParams(lParams);
	}
	
	@Override
	protected void onUserLeaveHint()
	{
		// TODO Auto-generated method stub
		super.onUserLeaveHint();
	}
	
	@Override
	protected void onPause() 
	{
	
		super.onPause();
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		swapLayout(R.layout.layout_blank); // 온에어의 updater handler 중지하기 위해서.. ㅡㅡ;;(꼼수.)
		
		Intent intent = new Intent(this, TimerService.class);
		stopService(intent);
		
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
		try 
		{
			System.exit(0);
		} 
		catch(Exception e) 
		{
//			Log.e(TAG, "System.exit(0) error - "+ e.getLocalizedMessage());
		}
		
	}
	
	private static final int MSG_TIMER_EXPIRED = 1;
	private static final int BACKKEY_TIMEOUT = 2;
	private static final int MILLIS_IN_SEC = 1000;
	private boolean mIsBackKeyPressed = false;
	private long mCurrTimeInMillis = 0;

	@SuppressWarnings("deprecation")
	@Override
    public void onBackPressed () 
	{
		
		if(baseMenu.isMenuShowing())
		{
			baseMenu.toggle();
		}
		else if(getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
		{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		else
		{
			String currentMenu = ((BandiApplication) getApplication()).getCurrentMenu();
			if (currentMenu.equals("OnAir") || currentMenu.equals("")) 
			{
				if (mIsBackKeyPressed == false)
				{
		            mIsBackKeyPressed = true;
		            mCurrTimeInMillis = Calendar.getInstance().getTimeInMillis();
		            Toast t = Toast.makeText(this, "'뒤로' 버튼을 한 번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
		            t.setGravity(Gravity.TOP, 0, 110);
		            t.show();
		            
		            startTimer();
		        } 
				else
				{
		            mIsBackKeyPressed = false;
	
		            if (Calendar.getInstance().getTimeInMillis() <= (mCurrTimeInMillis + (BACKKEY_TIMEOUT * MILLIS_IN_SEC))) 
		            {
		            	Session session = Session.getActiveSession();
		                if(session != null) session.closeAndClearTokenInformation();
		                
		                finish();
		                finishActivity();
		                ActivityManager am = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
//		                am.restartPackage(getPackageName());
		                am.killBackgroundProcesses(getPackageName()); //need KILL_BACKGROUND_PROCESSES permission  
		            }
		        }
				
			} 
			else if(currentMenu.equals("LoginForm"))
			{
				
				if(bandiApp.getLoginPrevView().equals("bandiBoard"))
				{
					findViewById(R.id.layout_menu3).performClick();
				}
				else
				{
					findViewById(R.id.layout_menu1).performClick();
				}
			}
			else 
			{
				findViewById(R.id.layout_menu1).performClick();
			}
		}
	}
	
	private void startTimer() 
	{
		mTimerHandler.sendEmptyMessageDelayed(MSG_TIMER_EXPIRED, BACKKEY_TIMEOUT * MILLIS_IN_SEC);
	}
	 
	private Handler mTimerHandler = new Handler() 
	{
		@Override
		public void handleMessage(Message msg) 
		{
			// super.handleMessage(msg);
	        switch (msg.what) 
	        {
		        case MSG_TIMER_EXPIRED: 
		        {
		           mIsBackKeyPressed = false;
		        }
	
	            break;
	        }
	    }
	};
		
	
	/**
	 * 콘텐츠 변경 영역에 메뉴에 따라 layout 구성 변경.
	 * @param layoutResID
	 */
	protected void swapLayout(int layoutResID) 
	{
		BaseActivity.swapLayout(this, layoutResID);
	    if (baseMenu.isMenuShowing()) baseMenu.toggle();
	}
	
	/**
	 * 클래스 외부에서 제목 영역 교체 시 호출
	 * @param context
	 * @param layoutResID
	 */
	public static void swapLayout(Context context, int layoutResID) 
	{
		RelativeLayout.LayoutParams rParams;
		LinearLayout.LayoutParams lParams; 
		
		Activity  activity = (Activity) context;
		FrameLayout swapZone = (FrameLayout) activity.findViewById(R.id.menu_swap_zone);
        swapZone.removeAllViews();
        
        LinearLayout scroll = (LinearLayout)activity.findViewById(R.id.layout_subScroll);
        FrameLayout content = (FrameLayout)activity.findViewById(R.id.content);
        FrameLayout boardText = (FrameLayout)activity.findViewById(R.id.layout_board_text);
        LinearLayout bottomBtnGroup = (LinearLayout)activity.findViewById(R.id.layout_bottomBtnGroup);
        LinearLayout bottomMenu = (LinearLayout)activity.findViewById(R.id.bottomMenu);
        FrameLayout onAirTools = (FrameLayout)activity.findViewById(R.id.onAirTools);
        LinearLayout topBtnGroup = (LinearLayout)activity.findViewById(R.id.topBtnGroup);
        FrameLayout onAirCheck = (FrameLayout)activity.findViewById(R.id.checkBox_viewRadio);
        FrameLayout appImg2 = (FrameLayout)activity.findViewById(R.id.layout_appImg2);
        FrameLayout replayImg = (FrameLayout)activity.findViewById(R.id.layout_replayImg);
        FrameLayout videoView = (FrameLayout)activity.findViewById(R.id.layout_mainVideoView);
        RelativeLayout textZone = (RelativeLayout)activity.findViewById(R.id.text_zone);
		RelativeLayout writeZone = (RelativeLayout)activity.findViewById(R.id.write_zone);
		
        
        videoView.setVisibility(View.GONE);
        replayImg.setVisibility(View.INVISIBLE);
        
        textZone.setVisibility(View.VISIBLE);
    	writeZone.setVisibility(View.INVISIBLE);
        
    	if(onAirCheck.isSelected())
    	{
    		onAirCheck.performClick();
    	}
//        onAirCheck.setSelected(false);
        
        FrameLayout[] tabMenuBtn = new FrameLayout[4];
        int[] tabMenuBtnRes = {R.id.tabBtn0, R.id.tabBtn1, R.id.tabBtn2, R.id.tabBtn3};
        
        if(layoutResID == R.layout.layout_air_schedule || layoutResID == R.layout.bandiboard)
        {
        	topBtnGroup.setVisibility(View.VISIBLE);
        	onAirTools.setVisibility(View.GONE);
        	appImg2.setVisibility(View.INVISIBLE);
        	RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params1.setMargins(0, 0, 0, 0);
			bottomMenu.setLayoutParams(params1);
			       	
        	lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
        	scroll.setLayoutParams(lParams);
        	
        	lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
        	content.setLayoutParams(lParams);
        	
        	
        	boardText.setVisibility(View.GONE);
        	bottomBtnGroup.setVisibility(View.GONE);
        	
        	
        	rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        	rParams.addRule(RelativeLayout.BELOW, R.id.top);
        	rParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard1);
        	swapZone.setLayoutParams(rParams);
        }
        else if(layoutResID == R.layout.layout_on_air)
        {
        	topBtnGroup.setVisibility(View.VISIBLE);
        	boardText.setVisibility(View.VISIBLE);
        	bottomBtnGroup.setVisibility(View.VISIBLE);
        	onAirTools.setVisibility(View.VISIBLE);
        	appImg2.setVisibility(View.INVISIBLE);
        	for(int i = 0; i < 4; i++)
    		{
    			tabMenuBtn[i] = (FrameLayout)activity.findViewById(tabMenuBtnRes[i]);
    			tabMenuBtn[i].setTag(i);
    			tabMenuBtn[i].setSelected(false);
    		}
        	
        	rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        	rParams.addRule(RelativeLayout.BELOW, R.id.top);
        	rParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard);
        	swapZone.setLayoutParams(rParams);
        	
        }
        else if(layoutResID == R.layout.layout_noti_and_event || layoutResID == R.layout.layout_favorite || layoutResID == R.layout.layout_setting || layoutResID == R.layout.login || layoutResID == R.layout.layout_intro_apps)
        {
        	
        	topBtnGroup.setVisibility(View.GONE);
        	onAirTools.setVisibility(View.GONE);
        	appImg2.setVisibility(View.INVISIBLE);
        	RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params1.setMargins(0, 0, 0, 0);
			bottomMenu.setLayoutParams(params1);
			
			
        	
        	lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
        	scroll.setLayoutParams(lParams);
        	
        	lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
        	content.setLayoutParams(lParams);
        	
        	
        	boardText.setVisibility(View.GONE);
        	bottomBtnGroup.setVisibility(View.GONE);
        	
        	rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        	rParams.addRule(RelativeLayout.BELOW, R.id.top);
        	rParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard1);
        	swapZone.setLayoutParams(rParams);
        }
        
        
        View view = (View) activity.getLayoutInflater().inflate(layoutResID, swapZone);
        
	}
	
	
	/**
	 * 타이틀 바 메뉴 제목 적용
	 * @param stringResID
	 */
	protected void swapTitle(int stringResID)
	{
		BaseActivity.swapTitle(this, stringResID);
	}
	
	/**
	 * 클래스 외부에서 제목 영역 교체 시 호출
	 * @param context
	 * @param stringResID
	 */
	public static void swapTitle(Context context, int stringResID) 
	{
		Activity activity = (Activity) context;
		TextView title = (TextView) activity.findViewById(R.id.title_text);
		title.setText(stringResID);
	}
	
	
	/**
	 * 좌측 슬라이딩 메뉴 적용
	 * @param context
	 */
	
	protected void setSlidingMenu(Context context) 
	{
		
		baseMenu = new SlidingMenu(context);
        baseMenu.setMode(SlidingMenu.LEFT);
        baseMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        //baseMenu.setShadowWidthRes(R.dimen.shadow_width);    
        //baseMenu.setShadowDrawable(R.drawable.shadow);
//        baseMenu.setBehindWidthRes(R.dimen.slidingmenu_width);
        baseMenu.setBehindWidth(dm.widthPixels / 2);
        //baseMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset); // 메뉴 우측 남는 공간
        //baseMenu.setAboveOffsetRes(R.dimen.slidingmenu_aboveoffset);
        baseMenu.setFadeDegree(0.35f);
        baseMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        baseMenu.setMenu(R.layout.menu);
        
        //baseMenu.showMenu();
        //baseMenu.toggle();
        
        /*
         * slide menu     
         */
        // 온에어
        final TextView on_air = (TextView) findViewById(R.id.on_air);
        final ImageView icon_on_air = (ImageView)findViewById(R.id.imageView_on_air);
        //final LinearLayout menuLayout1 = (LinearLayout)findViewById(R.id.layout_menu1);
        findViewById(R.id.layout_menu1).setOnClickListener(new OnClickListener() 
        {
	        public void onClick(View v) 
	        {
	        	if(on_air.isSelected() == true)
	        	{
	        		if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        	}
	        	else
	        	{
		        	unselectAllMenus(BaseActivity.this);
		        	on_air.setSelected(true);
		        	icon_on_air.setSelected(true);
		        	v.setBackgroundColor(Color.parseColor("#3f3833"));
		        	new OnAir(BaseActivity.this).run();
		        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        	}
	        }
	    });
        findViewById(R.id.layout_menu1).performClick(); // 초기 선택
	    
        // 편성표
        final TextView air_schedule = (TextView) findViewById(R.id.air_schedule);
        final ImageView icon_air_schedule = (ImageView)findViewById(R.id.imageView_air_schedule);
        //final LinearLayout menuLayout2 = (LinearLayout)findViewById(R.id.layout_menu2);
        findViewById(R.id.layout_menu2).setOnClickListener(new OnClickListener() 
        {
	        public void onClick(View v)
	        {
	        	unselectAllMenus(BaseActivity.this);
	        	air_schedule.setSelected(true);
	        	icon_air_schedule.setSelected(true);
	        	v.setBackgroundColor(Color.parseColor("#3f3833"));
        		new AirSchedule(BaseActivity.this).run();
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
        
        //반디게시판
        final TextView bandi_board = (TextView) findViewById(R.id.bandi_board_menu);
        final ImageView icon_bandi_board = (ImageView)findViewById(R.id.imageView_bandi_board_menu);
        //final LinearLayout menuLayout3 = (LinearLayout)findViewById(R.id.layout_menu3);
        findViewById(R.id.layout_menu3).setOnClickListener(new OnClickListener() 
        {
	        public void onClick(View v) {
	        	unselectAllMenus(BaseActivity.this);
	        	bandi_board.setSelected(true);
	        	icon_bandi_board.setSelected(true);
	        	v.setBackgroundColor(Color.parseColor("#3f3833"));
        		new BandiBoard(BaseActivity.this).run();
        		
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
        
        //패밀리서비스
        final TextView family_service = (TextView) findViewById(R.id.family_service);
        final ImageView icon_family_service = (ImageView)findViewById(R.id.imageView_family_service);
        //final LinearLayout menuLayout4 = (LinearLayout)findViewById(R.id.layout_menu4);
        findViewById(R.id.layout_menu6).setOnClickListener(new OnClickListener() 
        {
	        public void onClick(View v) 
	        {
	        	
	        	
	        	unselectAllMenus(BaseActivity.this);
	        	family_service.setSelected(true);
	        	icon_family_service.setSelected(true);
	        	v.setBackgroundColor(Color.parseColor("#3f3833"));
        		new FamilyService(BaseActivity.this).run();
        		
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
        
        // 공지/이벤트
        final TextView notiAndEvent = (TextView) findViewById(R.id.noti_and_event);
        final ImageView icon_notiAndEvent = (ImageView)findViewById(R.id.imageView_noti_and_event);
        findViewById(R.id.layout_menu5).setOnClickListener(new OnClickListener() 
        {
	        public void onClick(View v) 
	        {
	        	unselectAllMenus(BaseActivity.this);
	        	notiAndEvent.setSelected(true);
	        	icon_notiAndEvent.setSelected(true);
	        	v.setBackgroundColor(Color.parseColor("#3f3833"));
	        	new NotiAndEvent(BaseActivity.this).run();
	        	
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
        
        // 즐겨찾기
        final TextView favorite = (TextView) findViewById(R.id.favorite);
        final ImageView icon_favorite = (ImageView)findViewById(R.id.imageView_favorite);
        
        findViewById(R.id.layout_menu4).setOnClickListener(new OnClickListener()
        {
	        public void onClick(final View v) 
	        {
	        	
	        	if(isNeededToLogin())
	        	{
	        		Log.d(TAG, "isNeededToLogin");
	        		LoginFormDialog loginForm = LoginFormDialog.getInstance(BaseActivity.this);
					loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb() 
					{
						@Override
						public void onLoginSuccess() 
						{
							unselectAllMenus(BaseActivity.this);
				        	favorite.setSelected(true);
				        	icon_favorite.setSelected(true);
				        	v.setBackgroundColor(Color.parseColor("#3f3833"));
				        	new Favorite(BaseActivity.this).run();
						}
					});
					loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb() 
					{
						@Override
						public void onLoginFailure() 
						{
							// pass
						}
					});
					
					loginForm.showDialog(true);
	        	}
	        	else
	        	{
		        	Log.d(TAG, "isNeededToLogin else");
	        		unselectAllMenus(BaseActivity.this);
		        	favorite.setSelected(true);
		        	icon_favorite.setSelected(true);
		        	v.setBackgroundColor(Color.parseColor("#3f3833"));
		        	new Favorite(BaseActivity.this).run();
	        	}
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
        
        // 설정
        final TextView setting = (TextView) findViewById(R.id.setting);
        final ImageView icon_setting = (ImageView)findViewById(R.id.imageView_setting);
        //final LinearLayout menuLayout7 = (LinearLayout)findViewById(R.id.layout_menu7);
        findViewById(R.id.layout_menu7).setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	unselectAllMenus(BaseActivity.this);
	        	setting.setSelected(true);
	        	icon_setting.setSelected(true);
	        	v.setBackgroundColor(Color.parseColor("#3f3833"));
	        	new Setting(BaseActivity.this).run();
	        	
	        	if (baseMenu.isMenuShowing()) baseMenu.toggle();
	        }
	    });
 
	}
	
	
	
	/**
	 * 슬라이딩 메뉴의 모든 메뉴를 선택하지 않는 것으로 처리.
	 */
	public static void unselectAllMenus(Activity activity) 
	{
		int[] menuIds = {R.id.on_air, R.id.air_schedule, R.id.bandi_board_menu, R.id.family_service, R.id.noti_and_event, R.id.favorite, R.id.setting};
		int[] iconIds = {R.id.imageView_on_air, R.id.imageView_air_schedule, R.id.imageView_bandi_board_menu, R.id.imageView_family_service, R.id.imageView_noti_and_event, R.id.imageView_favorite, R.id.imageView_setting};
		int[] layoutIds = {R.id.layout_menu1, R.id.layout_menu2, R.id.layout_menu3, R.id.layout_menu4, R.id.layout_menu5, R.id.layout_menu6, R.id.layout_menu7};
		String[] layoutColor = {"#9e9188", "#898077", "#9e9188", "#ac9e93", "#9e9188", "#898077", "#7d746b"};
		
		for (int id : menuIds) 
		{
			TextView view = (TextView) activity.findViewById(id);
			view.setSelected(false);
		}
		
		for(int id : iconIds)
		{
			ImageView icon = (ImageView) activity.findViewById(id);
			icon.setSelected(false);
		}
		
		for(int i = 0; i < layoutIds.length; i++)
		{
			LinearLayout layout = (LinearLayout) activity.findViewById(layoutIds[i]);
			layout.setBackgroundColor(Color.parseColor(layoutColor[i]));
		}
		
		
	}
	
	public boolean isNeededToLogin()
	{
//		Login login = new Login(this);
		LoginApi login = new LoginApi(this);
		return login.isNeededToLogin();
		
//		boolean needToLogin = true;
//		
//		if ("".equals( bandiApp.getUserId() )) 
//		{
//			Preferences prefs = new Preferences(this);
//            if (prefs.getAutoLogin()) 
//            {
//                if (!("".equals(prefs.getUserId()) || "".equals(prefs.getPassword())))
//                {
//                    Login lg = new Login(this);
//                    if (lg.login(prefs.getUserId(), prefs.getPassword()) ) 
//                    {
//                        needToLogin = false;
//                    }
//                }
//            }
//        } 
//		else
//		{
//            needToLogin = false;
//        }
//		
//		return needToLogin;
	}

	public int getWidthDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, getResources().getDisplayMetrics());
	}
	
	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
	
	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;
	}
	
	/**
	 * 2017-04-10 현재 기능 없음 - MainActivity 에서 override 하므로 그냥 둠
	 * 
	 */
	protected void finishActivity()
	{
		
	}
}
