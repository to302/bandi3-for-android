package kr.ebs.bandi;

import java.util.Calendar;

import kr.ebs.bandi.data.ScheduleCompare;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;


/**
 * 반디3에서 재작성.
 * 
 * @since 2015. 6. 18.
 */
public class TimeTable 
{
    private final String TAG = "TimeTable"; // for Log
    
    private Context context;
    private BandiApplication bandiApp;
    private Preferences prefs;
    
    static final int FM = 1;
    static final int IRADIO = 2;
    
    
    public TimeTable(Context context)
    {
        this.context = context;
        this.bandiApp = (BandiApplication) context.getApplicationContext();
        this.prefs = new Preferences(context);
    }
    
    
    /**
     * FM 편성표를 서버에서 받아와서 저장한다. <BR>
     * TimeTable.FM , TimeTable.IRADIO 값을 변수로 받는다.<br>
     * 저장 성공 시, true 반환 오류 발생 시 false 반환
     */
    public boolean retrieve(int channel)
	{
    	Log.d(TAG, "do retrieve(" + channel +")");
    	
    	String url = (channel==FM ? Url.FM_TIMETABLE_JSON : channel==IRADIO ? Url.IRADIO_TIMETABLE_JSON : "");
    	if ("".equals(url)) return false;
		
    	String jsonStr = Url.getServerText(url);
		JSONObject jObject;
		
		try 
		{
			jObject = new JSONObject(jsonStr);
			
			if(jObject != null)
			{
				JSONArray jArray = jObject.getJSONObject("channels").getJSONArray("programs");
				if (channel == FM) bandiApp.setRadioData(jArray);
				else if (channel == IRADIO) bandiApp.setIRadioData(jArray);
			}
		} 
		catch (JSONException e) 
		{
			Log.e(TAG, "retrieve : " + e.getMessage());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

    
    /**
     * fm, i-radio 두 채널 편성표 모두를 가져와, <br> 
     * 시작 채널을 확인하여 ScheduleCompare class 의 데이타를 설정한다. <br>
     * @return
     */
    public boolean retrieveAndSetStartChannel()
    {
    	boolean fm = retrieve(FM);
    	boolean iradio = retrieve(IRADIO);
    	
    	if (fm && iradio)
    	{
    		if (prefs.getIsStartFMRadio()) ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
			else ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
    	}
    	
    	return (fm && iradio);
    }
    
 


    
//////////// 반디2 의 유산 ////////////////////////////////////////////////

    /**
     * 편성표용 날짜 변경 보정 시간. (익일 2시에 편성표 호출날짜 변경)
     */
    public static final int HOUR_ADJUSTMENT = -2;

    /**
     * xml file 을 받아오기 위한 날짜 string 을 반환 (예.20120312) <br />
     * 라디오 방송 종료시간을 고려해서 2시간 늦은 날짜를 반영. <br />
     * (2월 2일 01시 29분 일 경우 -> 2월 1일 날짜로 반환)
     * @return
     */
    public String getDateForXmlRetrieving() 
    {
        // 오늘 날짜를 기준으로..
        Calendar cal = DateUtil.getKoreanCalendar();
        cal.add(Calendar.HOUR_OF_DAY, HOUR_ADJUSTMENT); // 라디오 방송 종료 시간 (익일 새벽 2시)

        return DateUtil.get8DigitsDateString(cal);
    }

}
