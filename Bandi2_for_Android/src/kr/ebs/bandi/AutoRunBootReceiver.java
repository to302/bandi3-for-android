package kr.ebs.bandi;

import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoRunBootReceiver extends BroadcastReceiver {
	static final String TAG = "AutoRunBootReceiver";
	
	Preferences prefs ;
	
	public AutoRunBootReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		
		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			Log.d(TAG, "onReceiver running.");
			try {
				Context ctx = context.getApplicationContext();
				this.prefs = new Preferences(ctx);
				Setting setting = new Setting(ctx, this.prefs);
				setting.reserveAutoRun();
			}catch(Exception e) {
				Log.e(TAG, "AutoRunBootReceiver.onReceive Error.");
				e.printStackTrace();
			}
		}
	}
	
}
