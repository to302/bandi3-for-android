package kr.ebs.bandi;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;

public class TwitterDialog extends Dialog 
{
	Context context;
	EditText inputBox;
	Button confirm, cancel;
	OnTwitterDialogListener listener;
	public TwitterDialog(Context context)
	{
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	public void setListener(OnTwitterDialogListener listener)
	{
		this.listener = listener;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_twitter);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.dimAmount = 0.7f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
		
		inputBox = (EditText)findViewById(R.id.editText_twitter);
		inputBox.setText("신개념 EBS 인터넷 라디오 - EBS 반디 http://me2.do/FDTk7Pyi");
	
		confirm = (Button)findViewById(R.id.button_twitConfirm);
		cancel = (Button)findViewById(R.id.button_twitCancel);
		
		confirm.setOnClickListener(clickListener);
		cancel.setOnClickListener(clickListener);
		
	}
	
	android.view.View.OnClickListener clickListener = new android.view.View.OnClickListener() 
	{
		
		@Override
		public void onClick(View v) 
		{
			// TODO Auto-generated method stub
			if(v.getId() == R.id.button_twitConfirm)
			{
				dismiss();
				listener.OnConfirmClick(inputBox.getText().toString());
				
			}
			else if(v.getId() == R.id.button_twitCancel)
			{
				dismiss();
				listener.OnCancelClick();
				
			}
		}
	};
	
	public interface OnTwitterDialogListener
	{
		public void OnConfirmClick(String message);
		public void OnCancelClick();
	}
}
