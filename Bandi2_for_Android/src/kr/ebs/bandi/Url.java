package kr.ebs.bandi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import kr.ebs.bandi.data.CProgramData;

import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONObject;
import org.w3c.dom.Document;

import android.util.Log;
import android.util.Base64;

public class Url
{
	private static final String TAG = "Url";

	/*
	 * 스트리밍 URL 소스 고정값, (서버 API 에서 제공받지 못할 때 사용된다.)
	 */
	// FM
	static final String fmAudioStreamHttp = "http://ebsonairios.ebs.co.kr/fmradiobandiaodhls/bandiappaachls/playlist.m3u8";
	static final String fmAudioStreamRtsp = "rtsp://ebsandroid.ebs.co.kr/fmradiobandiaod/bandiappaac";
	/**
	 * iOS 용 FM 스트리밍 (HLS) (Audio only)
	 */
	static final String fmAudioStreamHLS = "http://ebsonairiosaod.ebs.co.kr/fmradiobandiaod/bandiappaac/playlist.m3u8";
	
	// FM - 보이는 라디오
	static final String fmVideoStreamRtsp = "rtsp://ebsandroid.ebs.co.kr/fmradiotablet500k/tablet500k";

	// i-Radio
	static final String iRadioAudioStreamHttp = "http://new_iradio.ebs.co.kr/iradio/iradiolive_m4a/playlist.m3u8";
	static final String iRadioAudioStreamRtsp = "rtsp://new_iradio.ebs.co.kr/iradio/iradiolive_m4a";
	/**
	 * iOS 용 i-radio 스트리밍 (HLS) (Audio only)
	 */
	static final String iRadioAudioStreamHLS = "http://new_iradio.ebs.co.kr/iradio_ios/iradiolive_m4a/playlist.m3u8";

	/**
	 * 서버에서 제공하는 스트리밍 URL 정보 (xml)
	 */
	static final String STREAM_INFO_XML = "http://home.ebs.co.kr/bandi/onairUrl?fileType=xml";

	/**
	 * 서버에서 제공하는 스트리밍 URL 정보 (xml)
	 */
	static final String STREAM_INFO_JSON = "http://home.ebs.co.kr/bandi/onairUrl?fileType=json";

	/**
	 * FM 편성정보 (xml)
	 */
	static final String FM_TIMETABLE_XML = "http://home.ebs.co.kr/bandi/broadcastXml?broadType=radio&fileType=xml";

	/**
	 * FM 편성정보 (json)
	 */
	static final String FM_TIMETABLE_JSON = "http://home.ebs.co.kr/bandi/broadcastXml?broadType=radio&fileType=json";

	/**
	 * i-Radio 편성정보 (xml)
	 */
	static final String IRADIO_TIMETABLE_XML = "http://home.ebs.co.kr/bandi/broadcastXml?broadType=iradio&fileType=xml";

	/**
	 * i-Radio 편성정보 (JSON)
	 */
	static final String IRADIO_TIMETABLE_JSON = "http://home.ebs.co.kr/bandi/broadcastXml?broadType=iradio&fileType=json";


	/**
	 * 반디 업그레이드 유도 정보 확인 (xml)
	 */
	static final String UPGRADE_CHECK_XML = "http://home.ebs.co.kr/bandi/appVerInfo?appNm=bandi&appDsCd=02&fileType=xml";

	/**
	 * 반디 업그레이드 유도 정보 확인 (json)
	 */
	static final String UPGRADE_CHECK_JSON = "http://home.ebs.co.kr/bandi/appVerInfo?appNm=bandi&appDsCd=02&fileType=json";
//	static final String UPGRADE_CHECK_JSON = "http://to302.phps.kr/test/appVerInfo.json";

	/**
	 * 공지/이벤트 데이타 (xml)
	 */
	static final String EVENT_DATA_XML = "http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=02&fileType=xml";

	/**
	 * 공지/이벤트 데이타 (json)
	 */
	static final String EVENT_DATA_JSON = "http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=02&fileType=json";
//	static final String EVENT_DATA_JSON = "http://to302.phps.kr/test/bandiEventNoticeList.json";

	/**
	 * FM 편성표 모바일 웹 페이지
	 */
	static final String FM_TIMETABLE_HTML = "http://m.ebs.co.kr/appChannel?appId=bandinew&channel=RADIO";
	
	/**
	 * i-Radio 편성표 모바일 웹 페이지
	 */
	static final String IRADIO_TIMETABLE_HTML = "http://m.ebs.co.kr/appChannel?appId=bandinew&channel=IRADIO";
	
	/**
	 * 반디 게시판 이벤트,공지글 (JSON)
	 */
	static final String BANDIBOARD_NOTICE_JSON = "http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=02&fileType=json";
	
	/**
	 * 반디 게시판 이벤트,공지글  (xml)
	 */
	static final String BANDIBOARD_NOTICE_XML = "http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=02&fileType=xml";
	
	/**
	 * 패밀리서비스 메뉴용 모바일 웹 페이지 URL
	 */
	static final String FAMILY_SERVICE_HTML = "http://m.ebs.co.kr/familyService?appYn=bandi";
	
	/**
	 * 새로운 로그인 API url <br>
	 * @param j_username - 아이디 <br>
	 * @param j_password - 패스워드 <br>
	 * @param SAMLRequest - base64 인코딩된 XML 파일 구조<br> 
	 * @since 2017-04-26
	 */
	static final String LOGIN_HTTPS_POST = "https://sso.ebs.co.kr/idp/profile/SAML2/POST-GET/SSO";
	
	/**
	 * 새로운 로그인 API 에서 사용될 SAMLRequest 형식의 base64 인코딩된 XML 문자열 반환
	 * @return
	 * @since 2017-04-26
	 */
	static final String SAMLRequest() {
		String Issuer = "ebs.bandi.app";
		String ID = Issuer + "-" +System.currentTimeMillis();
		String IssueInstant = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
		String xml = "<?xml version='1.0' encoding='UTF-8'?> ";
		xml += " <saml2p:AuthnRequest ";
		xml += " xmlns:saml2p='urn:oasis:names:tc:SAML:2.0:protocol' ";
		xml += " AssertionConsumerServiceURL='' ";
		xml += " Destination='"+ LOGIN_HTTPS_POST +"' ";
		xml += " ID='"+ ID +"' ";
		xml += " IssueInstant='"+ IssueInstant +"' ";
		xml += " Version='2.0'> ";
		xml += " <saml2:Issuer xmlns:saml2='urn:oasis:names:tc:SAML:2.0:assertion'>"+ Issuer +"</saml2:Issuer> ";
		xml += " </saml2p:AuthnRequest> ";
//		Log.d(TAG, "IssueInstant - " + IssueInstant);
//		Log.d(TAG, "ID - " + ID);
		return Base64.encodeToString(xml.getBytes(), Base64.DEFAULT);
	}
	
	/**
	 * 새로운 로그인 API 용 - sso 인증 여부 체크 (sso 만료 확인 후 재로그인) <br>
	 * 결과 코드 : 100 - 로그인 O, 101 - 로그인 X, 102 - 타 계정으로 로그인 상태 <br>
	 * @param userId
	 * @cookie 이전 인증 시 SSO에서 응답한 Cookie를 전송
	 * @since 2017-04-26
	 */
	static final String LOGIN_CHECK_ALIVE_HTTPS_GET = "https://sso.ebs.co.kr/idp/check.jsp";
	
	
	/**
	 * 어플리케이션에서 웹 브라우저를 실행할 때 인증한 상태 그대로 <br> 
	 * 웹 서비스를 이용하고자 하는 경우 사용하는 방식입니다.<br>
	 * @param returnUrl - 로그인을 유지한 체 오픈을 원하는 웹 URL
	 * @cookie 이전 인증 시 SSO에서 응답한 Cookie를 전송
	 * @since 2017-04-26
	 */
	static final String LOGIN_OPEN_WEB_HTTPS_GET = "https://sso.ebs.co.kr/idp/ott-forward.jsp";
	
	/**
	 * 회원탈퇴 URL <br>
	 * 로그인 할 때 받은 쿠키값이 설정된 상태에서 열어야 정상적으로 보여진다. <br>
	 */
	static final String SSO_WITHDRAW_URL = "https://sso.ebs.co.kr/idp/secession/auth.jsp?appYn=Y";
	
	/**
	 * 반디 게시물 초기 목록 API (xml)
	 * @param programId
	 * @return
	 */
	static final String BANDIBOARD_LIST_XML(String programId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiBoardList?fileType=xml&listType=L&pageSize=10&programId=%s", programId);
	}
	
	/**
	 * 반디 게시물 초기 목록 API (json)
	 * @param programId
	 * @return
	 */
	static final String BANDIBOARD_LIST_JSON(String programId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&listType=L&pageSize=10&programId=%s", programId);
	}
	
	
	/**
	 * 반디 게시물 더보기(추가) 게시물 목록 API (json)
	 * @param programId
	 * @param seq
	 * @return
	 */
	static final String BANDIBOARD_LIST_ADD_JSON(String programId, long seq)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&listType=N&pageSize=10&seq=%d&programId=%s", seq, programId);
	}
	
	/**
	 * 반디 게시물 더보기(추가) 게시물 목록 API (xml)
	 * @param programId
	 * @param seq
	 * @return
	 */
	static final String BANDIBOARD_LIST_ADD_XML(String programId, long seq)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiBoardList?fileType=xml&listType=N&pageSize=10&seq=%d&programId=%s", seq, programId);
	}
	
	/**
	 * 즐겨찾기 목록 API (json) <br>
	 * http://home.ebs.co.kr/bandi/bandiAppUserFbmkList?fileType=json&userId=%s
	 * @param userId
	 * @return
	 */
	static final String FAVORITE_LIST_JSON(String userId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiAppUserFbmkList?fileType=json&userId=%s", userId);
	}
	
	/**
	 * 즐겨찾기 목록 API (xml)
	 * @param userId
	 * @return
	 */
	static final String FAVORITE_LIST_XML(String userId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiAppUserFbmkList?fileType=xml&userId=%s", userId);
	}
	
	/**
	 * 즐겨찾기 아이템 삭제 (json)
	 * @param userId
	 * @param programId
	 * @param fbmkSno - 즐겨찾기 아이템 고유번호
	 * @return
	 */
	static final String REMOVE_FAVORITE_ITEM_JSON(String userId, String programId, int fbmkSno)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify?fileType=json&userId=%s&programId=%s&fbmkSno=%d", userId, programId, fbmkSno);
	}
	
	/**
	 * 즐겨찾기 아이템 삭제 (xml)
	 * @param userId
	 * @param programId
	 * @param fbmkSno - 즐겨찾기 아이템 고유번호
	 * @return
	 */
	static final String REMOVE_FAVORITE_ITEM_XML(String userId, String programId, int fbmkSno)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify?fileType=xml&userId=%s&programId=%s&fbmkSno=%d", userId, programId, fbmkSno);
	}
	
	/**
	 * login 처리 페이지 uri
	 */
	static final String LOGIN_ACTION_URL = "https://sso.ebs.co.kr/idp/bandiLogin"; 
	
	/**
	 * 로그인 URL 반환 <br> 
	 * https://sso.ebs.co.kr/idp/bandiLogin?userid=[userId]&passwd=[passwd] <br>
	 * 
	 * @param userId
	 * @param passwd
	 * @return
	 */
	static final String LOGIN_URL(String userId, String passwd)
	{
		try {
			userId = URLEncoder.encode(userId, "utf-8");
			passwd = URLEncoder.encode(passwd, "utf-8");
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "URLEncoder.encode() Error.");
			e.printStackTrace();
		}
		
		return String.format("%s?userid=%s&passwd=%s", LOGIN_ACTION_URL, userId, passwd);
	}
	
	/**
	 * 특정 프로그램 정보 API URL 반환  (JSON type)
	 * @param programId
	 * @return
	 */
	static final String PROGRAM_INFO_JSON(String programId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiProgInfo?version=2&fileType=json&programId=%s", programId);
	}
	
	/**
	 * 특정 프로그램 정보 API URL 반환  (XML type)
	 * @param programId
	 * @return
	 */
	static final String PROGRAM_INFO_XML(String programId)
	{
		return String.format("http://home.ebs.co.kr/bandi/bandiProgInfo?version=2&fileType=xml&programId=%s", programId);
	}
	
	/**
	 * 해당 프로그램의 다시듣기 목록 반환 (JSON)
	 * @param programId
	 * @param pageIndex
	 * @return
	 */
	static final String AOD_LIST_JSON(String programId, int pageIndex)
	{
		return String.format("http://home.ebs.co.kr/bandi/replayList?fileType=json&pageVodFig=10&programId=%s&page=%d", programId, pageIndex);
	}
	
	/**
	 * 다시듣기 항목의 스트리밍 정보 반환.
	 * @param userId
	 * @param programId
	 * @param lectId
	 * @return
	 */
	static final String AOD_INFO_JSON(String userId, String programId, String lectId)
	{
		return String.format("http://home.ebs.co.kr/bandi/vodSteamInfo?fileType=json&userId=%s&programId=%s&lectId=%s", userId, programId, lectId);
	}
	

	
	
	/*
	 * 베이스 경로 (테스트, 운영 전환용, BandiLog 에서 처음 사용)
	 */
	static final String BASE_DOMAIN_HOME = "http://home.ebs.co.kr";
	static final String BASE_DOMAIN_M = "http://m.ebs.co.kr";


	/**
	 * 편성표 웹 페이지 (html) <br>
	 * 오늘   : http://s-m.ebs.co.kr/appChannel?channel=RADIO&appId=bandi#current <br>
	 * 특정일 : http://s-m.ebs.co.kr/appChannel?channel=RADIO&appId=bandi&onairDate=20131023 <br>
	 */
	static final String AIR_SCHEDULE = BASE_DOMAIN_M + "/appChannel?channel=RADIO&appId=bandi";

	/*
	 * 인트로 팝업 배너
	 */
	static final String POP_BANNER = "http://m.ebs.co.kr/appbanner?appId=bandi";

	/*
	 * 앱 업그레이드 체크 XML 확인
	 */
	static final String UPGRADE_CHECK = BASE_DOMAIN_M + "/appVer?appId=bandi&appOs=android";

	/**
	 * 반디 구글 플레이 마켓 주소
	 */
	static final String BANDI_MARKET_LINK = "market://details?id=kr.ebs.bandi";

	/*
	 * 선곡표 xml 파일 경로 (progcd 전달할 것)
	 */
	static final String SONGS_TABLE = BASE_DOMAIN_HOME + "/bandi/bandiSongList?progcd=";

	/*
	 * EBS 앱 소개 웹 페이지 (html)
	 */
	static final String INTRO_APPS = BASE_DOMAIN_M + "/familyService?appYn=Y";
	
	/*
	 * 반디 게시판(Html)
	 */
	static final String BANDI_BOARD = BASE_DOMAIN_HOME + "/bandi/bandiAppList?progcd=";

	/*
	 * 반디 게시물 작성
	 */
	static final String BANDI_BORAD_ACTION = BASE_DOMAIN_HOME + "/bandi/bandiAppPost";

	/**
	 * 다시듣기 링크 (xml)
	 */
	static final String AOD = BASE_DOMAIN_HOME + "/bandi/bandiMobileUrl?progcd=";
	
	
	/**
	 * 불량회원 정보 반환 URL <br>
	 * @param userId : EBS 회원 ID
	 * @param snsDsCd : 001-페이스북, 002-트위터, 006-카카오스토리
	 * @param snsUserId
	 * @return
	 */
	static final String BAD_MEMBER_JSON(String userId, String snsDsCd, String snsUserId) {
		return String.format("http://home.ebs.co.kr/bandi/badMmbInfo?fileType=json&userId=%s&snsDsCd=%s&snsUserId=%s",userId, snsDsCd, snsUserId);
	}


	private static Document doc = null;

	/**
	 * 원격지의 xml 을 Document class로 반환한다.
	 * @param urlStr
	 * @return Document or null
	 */
	public static Document getRemoteDoc(final String urlStr)
	{
		doc = null;

		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			        factory.setIgnoringComments(true);
			        DocumentBuilder builder;
					builder = factory.newDocumentBuilder();

					URL url = new URL( urlStr );
			        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			        doc = builder.parse(urlConnection.getInputStream());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		try
		{
			thread.start();
			thread.join();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return doc;
	} // end getRemoteDoc()


	/**
	 * (URL 접근 가능한) 서버에 있는 text file 을 읽어 String 형태로 반환 <br>
	 * charset : utf-8 <br>
	 *
	 * @param urlStr
	 * @return
	 */
	public static String getServerText(String urlStr)
	{
		return getServerText(urlStr, "utf-8");
	}




	/**
	 * (URL 접근 가능한) 서버에 있는 text file 을 읽어 String 형태로 반환
	 * @param urlStr
	 * @param charset
	 * @return
	 */
	public static String getServerText(final String urlStr, final String charset)
	{

		final StringBuilder sb = new StringBuilder();

		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				HttpURLConnection urlConnection = null;

				try
				{
				    URL url = new URL(urlStr);
				    urlConnection = (HttpURLConnection) url.openConnection();
				    BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), charset), 8192);
				    String inStr;

				    while ((inStr = in.readLine()) != null)
				    {
				        inStr = inStr.trim();
				        sb.append(inStr);
				    }

				    in.close();
				    urlConnection.disconnect();
				}
				catch (Exception e)
				{
				    e.printStackTrace();
				}
				finally
				{
				    if (urlConnection != null) urlConnection.disconnect();
				}
			}
		});

		thread.start();
		try
		{
			thread.join();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * @deprecated - 명확한 반환값을 위해 getHostIPv4 함수를 대신 사용할 것.<br/> 
	 *               (IPv6 형식을 반환하기도 함)
	 * @param hostname
	 * @return
	 */
	public static String getServerIP(final String hostname)
	{
		final StringBuilder sb = new StringBuilder();
		Thread thread = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				try
				{
					InetAddress inetAddress = InetAddress.getByName(hostname);
					sb.append(inetAddress.getHostAddress());
				}
				catch (UnknownHostException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});

		try
		{
			thread.start();
			thread.join();
		}
		catch(Exception e)
		{
			sb.append(hostname);
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	/**
	 * FQDN 정보를 받아서 해당 host 의 IPv4 값을 반환한다.
	 * @param hostname
	 * @return
	 */
	public static String getHostIPv4(final String hostname) 
	{
		final StringBuilder sb = new StringBuilder();
		
		Thread thread = new Thread() {
			@Override
			public void run()
			{
				try {
					InetAddress[] inetAddrs = InetAddress.getAllByName(hostname);
					
					for (int i=0; i<inetAddrs.length; i++) 
					{
						Log.d(TAG, ">> inetAddrs : " + inetAddrs[i].toString());
						if (! inetAddrs[i].isLoopbackAddress()
								&& InetAddressUtils.isIPv4Address(inetAddrs[i].getHostAddress()))
						{
							Log.d(TAG, ">> IPv4 : " + inetAddrs[i].getHostAddress());
							sb.append(inetAddrs[i].getHostAddress());
						}
					}
				} catch (Exception e) {
					Log.e(TAG, "getHostIPv4() error - " + e.getMessage());
					e.printStackTrace();
				}
			}
		};
		
		try
		{
			thread.start();
			thread.join();
		}
		catch(Exception e)
		{
			sb.append(hostname);
			e.printStackTrace();
		}
		
		return sb.toString();
	}


	public static boolean existsRemoteFile(final String urlStr)
	{
		boolean result = false;
		final int[] code = new int[1] ;

		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					URL url = new URL(urlStr);
					//URLConnection con = url.openConnection();
					HttpURLConnection exitCode = (HttpURLConnection) url.openConnection();;
					code[0] = exitCode.getResponseCode();

				}
				catch (UnknownHostException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});


		try
		{
			thread.start();
			thread.join();

			Log.d("bbb", code[0]  + "-aa");
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		if (code[0] == 200) result = true;

		return result;
	}

	
	/**
	 * HTTP 서버에 post 방식으로 데이타 전달 <br>
	 * 서버 응답 문자열 반환 <br>
	 * @param urlStr - http://...
	 * @param params - (key, value) 형식의 데이타 묶음 (Map)
	 * @param charset - 데이타 문자열 decode charset 
	 * @return 서버 반환 문자열
	 */
	public static String postHttpRequest(String urlStr, Map<String, ?> params, String charset)
	{
		String responseStr = "";
		HttpURLConnection con;
		URL url;
		DataOutputStream wr;
		
		if (params == null) params = new HashMap<String, Object>();
		
		StringBuilder sb = new StringBuilder();
		
		for (String key : params.keySet())
		{
			if (sb.length() != 0) sb.append("&");
			try {
				sb.append(key + "=" + URLEncoder.encode(params.get(key).toString(), charset));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String urlParameters = sb.toString();
		Log.d(TAG, "urlParameters : " + urlParameters);
		
		
		try 
		{
			url = new URL(urlStr);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
//			con.setRequestProperty("User-Agent", USER_AGENT);
//			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setDoOutput(true);
			con.setChunkedStreamingMode(0);
			
			if (! "".equals(urlParameters))
			{
				wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();
			}
			
			int responseCode = con.getResponseCode();
//			Log.d(TAG, "Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), charset), 8192);
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
//			Log.d(TAG, "responseStr : " + response.toString());
			responseStr = response.toString();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		return responseStr.trim();
	}
	
	/**
	 * 반디 게시물 등록용, 서버응답 문자열 반환<br>
	 * 전달 parameters <br>
	 * <pre>
	 * progcd : 프로그램 코드
	 * contents : 작성내용
	 * appDsCd : 02 (for android)
	 * broadType : {radio, iradio}
	 * userid : 로그인 ID (SNS 로그인 시 공백)
	 * writer : 작성자 명 (SNS 로그인 시 공백)
	 * -- SNS case --
	 * snsDsCd : {001(for facebook), 002(for twitter)}
	 * snsUserId : SNS 아이디 
	 * snsUserNm : SNS 이름
	 * snsUseYn : SNS 로그인 여부
	 * </pre>
	 * 
	 * @param params
	 * @return 서버 응답 문자열
	 */
	public static String postBandiBoardArticle(Map<String, ?> params)
	{
		String urlStr = "http://home.ebs.co.kr/bandi/bandiAppPost3"; // 게시물 등록 API url
//		String urlStr = "http://to302.phps.kr/test/badWord.xml"; // 게시물 등록 API url
		
		return postHttpRequest(urlStr, params, "UTF-8");
	}
	
	/**
	 * EBS ID 로 불량 회원 여부 정보 가져오기
	 * @param userId : EBS 회원 ID
	 * @return
	 */
	public static HashMap<String, String> getBadMemberInfo(String userId)
	{
		return getBadMemberInfo(userId, "", "");
	}
	
	/**
	 * sns login 시 불량회원 정보 가져오기
	 * @param snsDsCd : 001-페이스북, 002-트위터, 006-카카오스토리
	 * @param snsUserId
	 * @return
	 */
	public static HashMap<String, String> getBadMemberInfo(String snsDsCd, String snsUserId)
	{		
		return getBadMemberInfo("", snsDsCd, snsUserId);
	}
	
	/**
	 * 불량회원 정보 가져오기 (EBS 로그인이나 sns 로그인 둘 중에 하나만 지정)
	 * @param userId : EBS 회원 ID
	 * @param snsDsCd : 001-페이스북, 002-트위터, 006-카카오스토리
	 * @param snsUserId : sns 로그인 id
	 * @return
	 */
	public static HashMap<String, String> getBadMemberInfo(String userId, String snsDsCd, String snsUserId)
	{
		HashMap<String, String> map = new HashMap<String, String>();
		try {
			String jsonStr = Url.getServerText(BAD_MEMBER_JSON(userId, snsDsCd, snsUserId));
			if (!"".equals(jsonStr.trim())) {
				JSONObject jObj = (new JSONObject(jsonStr)).getJSONObject("data");
				String result = jObj.getString("result");
				map.put("result", result);	
				map.put("until", jObj.getString("until"));
				map.put("badMmbExp", jObj.getString("badMmbExp"));
				map.put("userId", jObj.getString("userId"));
				map.put("snsDsCd", jObj.getString("snsDsCd"));
				map.put("snsUserId", jObj.getString("snsUserId"));
			
				String update = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyyMMddHHmmss");
				Log.d(TAG, "update >> " + update +" / "+ map);
				map.put("update", update);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	
	
	
	

}
