package kr.ebs.bandi;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class NotificationService extends Service
{
	static final String TAG = "NotificationService";
	
	Notification notification;
    PendingIntent pi;
    NotificationManager notificationManager;
    BandiApplication bandiApp;
    public static NotificationService _this = null;
	@Override
	public void onCreate() 
	{

		super.onCreate();
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		_this = this;
		bandiApp = (BandiApplication)getApplication();
		setNotification();
//		return START_NOT_STICKY;
		return START_REDELIVER_INTENT;
	}
	
	@Override
	public IBinder onBind(Intent intent) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	private void setNotification() 
	{

		Intent intent = new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
      
		String appName = getResources().getString(R.string.app_name);      
		String progName = bandiApp.getTitleForBoard();

		notification = new NotificationCompat.Builder(getApplicationContext())
							.setContentTitle(appName)
							.setContentText((progName==null ? "" : progName))
							.setSmallIcon(getNotificationIcon())
							.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_bandi_launcher))
							.setTicker(appName)
							//.setAutoCancel(true) // 이것 설정하면 클릭하면 상단에 아이콘 사라짐.
							.setContentIntent(pi)
							.setOngoing(true) // 이게 없으면, 지우기 하면 사라진다. (주의)
							.build();
		
		
		startForeground(MainActivity.NOTIFICATION_ID, notification);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(MainActivity.NOTIFICATION_ID, notification);
		
		
	}
	
	/**
	 * 롤리팝 이상의 버전에서는 실루엣 아이콘을 반환. <br>
	 * 하위 버전에선 기존과 같이 컬러 아이콘 반환 <br>
	 * @return
	 */
	private int getNotificationIcon() {
	    boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
	    return whiteIcon ? R.drawable.ic_bandi_launcher_silhouette : R.drawable.ic_bandi_launcher;
	}
	
	
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		
		try 
		{
			stopForeground(true);
			
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    	notificationManager.cancel(MainActivity.NOTIFICATION_ID);
		} 
		catch(Exception e) {}
	}
}
