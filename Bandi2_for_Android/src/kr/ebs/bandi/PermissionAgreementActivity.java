package kr.ebs.bandi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kr.ebs.bandi.Preferences;

public class PermissionAgreementActivity extends Activity {
	static final String TAG = "PermissionAgreementActivity";
	
	Preferences prefs = null;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		prefs = new Preferences(this);
		
		if (this.prefs.getPermissionAgreement()) {
			this.jumpToBandiIntro();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_permission_agreement);
	}
	
	/**
	 * 허용 버튼 클릭 시 액션
	 * @param view
	 */
	public void doPermitOnClick(View view) {
		this.prefs.putPermissionAgreement(true);
		this.jumpToBandiIntro();
	}
	
	private void jumpToBandiIntro() {
		// 반디 인트로 화면으로 전환
		Intent intent = new Intent(this, BandiIntro.class);
		startActivity(intent);
		finish();
	}
	
    
}
