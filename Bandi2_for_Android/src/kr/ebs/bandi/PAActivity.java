package kr.ebs.bandi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class PAActivity extends Activity {
	
	static final String PREFS_NAME = "preferences";
	/**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    private final String PERM_AGREE = "permission_agreement";
	
	private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		if (this.getPermissionAgreement()) {
			this.jumpToBandiIntro();
		} else {
			confirm(this, "이용안내", "이 프로그램을 사용하시려면 카메라 권한의 허용이 필요합니다.", 
    		"허용하기", "허용하지 않음", 
    					new DialogInterface.OnClickListener() { // positive
							@Override
							public void onClick(DialogInterface dialog, int which) {
								PAActivity.this.putPermissionAgreement(true);
								jumpToBandiIntro();
							}
						},
						new DialogInterface.OnClickListener() { // negative
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Dialog.showAlert(PAActivity.this, "권한 허용이 되지 않아 앱이 종료됩니다.");
								finish();
							}
						} 
					); 
		}
		
		//setContentView(R.layout.activity_pa);
	}
	
	
	private void jumpToBandiIntro() {
		// 반디 인트로 화면으로 전환
		Intent intent = new Intent(this, BandiIntro.class);
		startActivity(intent);
		finish();
	}
	
    
    
    /**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    public void putPermissionAgreement(boolean isAgreed)
    {
    	editor = prefs.edit();
    	editor.putBoolean(PERM_AGREE, isAgreed);
    	editor.commit();
    }
    
    /**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    public boolean getPermissionAgreement()
    {
    	return prefs.getBoolean(PERM_AGREE, false);
    }

	private void confirm(Context context, String title, String msg, 
    		String positiveMsg, String negativeMsg, 
    		DialogInterface.OnClickListener positiveClickListener,
    		DialogInterface.OnClickListener negativeClickListener) 
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
               .setTitle(title)
               .setCancelable(false)
               .setNegativeButton(negativeMsg, negativeClickListener)
               .setPositiveButton(positiveMsg, positiveClickListener);
               
        AlertDialog alert = builder.create();
        alert.show();
    }
}
