package kr.ebs.bandi;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import android.content.Context;
import android.util.Base64;

/**
 * 원본소스 : http://www.imcore.net/encrypt-decrypt-aes256-c-objective-ios-iphone-ipad-php-java-android-perl-javascript-python/
 * 
 * @since 2016. 8. 8.
 */
public class AES256Cipher {
	public static byte[] ivBytes = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	/**
	 * AES 256 암호화 함수, 직접 사용하지 말고 encryptString(String, Context) 를 사용할 것.
	 * @param str
	 * @param key
	 * @return
	 * @throws java.io.UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String AES_Encode(String str, String password) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,	IllegalBlockSizeException, BadPaddingException {
		
		byte[] textBytes = str.getBytes("UTF-8");
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec newKey = generate256bitsKey(password); 
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
//		return Base64.encodeBase64String(cipher.doFinal(textBytes));
		return Base64.encodeToString(cipher.doFinal(textBytes), Base64.DEFAULT);
	}

	/**
	 * AES 256 복호화 함수, 직접 사용하지 말고 decryptString(String, Context) 를 사용할 것.
	 * @param str
	 * @param key
	 * @return
	 * @throws java.io.UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String AES_Decode(String str, String password) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		byte[] textBytes = Base64.decode(str, Base64.DEFAULT);
		//byte[] textBytes = str.getBytes("UTF-8");
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec newKey = generate256bitsKey(password);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
		return new String(cipher.doFinal(textBytes), "UTF-8");
	}
	
	/**
	 * AES 256 을 이용한 암호화 기능, 키는 Device ID 사용 
	 * @param str
	 * @param context
	 * @return
	 */
	public static String encryptString(String str, Context context) {
		String encStr = "";
		try {
			encStr = AES_Encode(str, BandiLog.getDeviceId(context));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encStr;
	}
	
	/**
	 * AES 256 을 이용한 복호화 기능, 키는 Device ID 사용
	 * @param str
	 * @param context
	 * @return
	 */
	public static String decryptString(String str, Context context) {
		String decStr = "";
		try {
			decStr = AES_Decode(str, BandiLog.getDeviceId(context));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decStr;
	}
	
	/**
     * key size 만큼 device id 값을 순서대로 채운다. 기기id 가 'abcd1234' 라면.. abcd1234abcd1234... 이렇게..
     * @return
     * @throws UnsupportedEncodingException
     */
    private static SecretKeySpec generate256bitsKey(final String password) throws UnsupportedEncodingException {
    	final String CHARSET = "UTF-8";
        StringBuilder sb = new StringBuilder();
        sb.append(password);
        while (sb.toString().getBytes(CHARSET).length < 32) {
            sb.append(password);
        }
        byte[] key = Arrays.copyOf(sb.toString().getBytes(CHARSET), 32); // 16 -> 128bit , 32 -> 256bit

        return new SecretKeySpec(key, "AES");
    }
}
