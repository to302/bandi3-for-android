package kr.ebs.bandi;


import android.os.IBinder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.VideoView;
import android.content.ComponentName;
import android.content.ServiceConnection;

/**
 * Singleton Pattern
 */
public class AudioServiceConnection implements ServiceConnection
{
	private volatile static AudioServiceConnection uniqueInstance;
	
	private FrameLayout playBtn = null;
	private ProgressBar loadingImage = null;
	private VideoView videoView = null;

	private AudioServiceConnection() 
	{
		// singleton pattern 적용.
	} 
	
	public static AudioServiceConnection getInstance() 
	{
		if (uniqueInstance == null) 
		{
			synchronized (AudioServiceConnection.class) 
			{
				if (uniqueInstance == null) 
				{
					uniqueInstance = new AudioServiceConnection();
				}
			}
		}
		return uniqueInstance;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	private AudioService audioService;
	private boolean isBound=false;
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service)
	{
		AudioService.ServiceBinder binder = (AudioService.ServiceBinder) service;
		audioService = binder.getService();
		isBound = true;
		
		if (playBtn != null && loadingImage != null) 
		{
			audioService.initPlayer(playBtn, loadingImage);
		}
		else
		{
			audioService.initPlayer();
		}
	}

	@Override
	public void onServiceDisconnected(ComponentName name)
	{
		audioService = null;
		isBound = false;
	}
	
	/**
	 * 서비스 바인딩 상태를 반환한다.
	 * @return isBound
	 */
	public boolean isBound() 
	{
		return isBound;
	}

	public AudioService getAudioService() 
	{
		return audioService;
	}

	
	/**
	 * 재생버튼과 접속 로딩 이미지 제어용.
	 * @param playBtn
	 * @param loadingImage
	 */
	public void setPlayLoadingBtn(FrameLayout playBtn, ProgressBar loadingImage)
	{
		this.playBtn = playBtn;
		this.loadingImage = loadingImage;
	}
	
	public void setVideoView(VideoView videoView)
	{
		this.videoView = videoView;
	}
	
	/**
	 * 
	 * @deprecated - 사용되지 않는 것으로 보임 on 2016.07.11 
	 * @param url
	 * @param url1
	 */
	public void switchPlayer(String url, String url1)
	{
		if(isBound)
		{
			audioService.switchUrl(url, url1);
		}
	}
	
	
}
