package kr.ebs.bandi;


import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.ScheduleCompare;
import kr.ebs.bandi.GoogleAnalytics;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;



public class OnAir implements BaseInterface 
{
	static final String TAG = "OnAir";
	BandiApplication bandiApp;
	public AudioServiceConnection serviceConnection;
	Preferences prefs;
	private Context context ;
	private Activity activity;
//	ImageView playBtn;
//	ProgressBar loading;
	
	FrameLayout topMenuBtn0;
	FrameLayout topMenuBtn1;
	ImageView progImage;
	ImageView icon;
	
	MainActivity main;
	String nowId, prevId;
	
		
	public OnAir(Context context) 
	{
		this.context = context;
		this.activity = (Activity) context;
		prefs = new Preferences(context);
		main = (MainActivity)activity;
		bandiApp = (BandiApplication)activity.getApplication();
		
		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);
		
		/*
		 * 접근 로그 저장
		 */
		ScheduleCompare.getInstance().init();
		if(bandiApp.getOnairHitInit() == false)
		{
			if(bandiApp.getIsFMRadio())
			{
				BandiLog.event(context, "OnAir");
			}
			else
			{
				BandiLog.event(context, "OnAirIradio");
			}
		}
		 
	}

	@Override
	public void run() 
	{
		BaseActivity.swapTitle(context, R.string.on_air);
		BaseActivity.swapLayout(context, R.layout.layout_on_air);
		
		setListener();
		
		/*
		 * 프로그램 이미지 updater 시작
		 * 보이는 라디오 버튼 클릭 시 오디오 스트림 제어 콜백 지정 (재생/멈춤 기능)
		 */ 
		new OnViewUpdater(context);
		
		//** Google Analytics **
		if (bandiApp.getIsFMRadio()) 
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.FM);
		} 
		else 
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.IRADIO);
		}
		
	}
	
	@SuppressLint("NewApi")
	private void setListener() 
	{
        serviceConnection = AudioServiceConnection.getInstance();
        topMenuBtn0 = (FrameLayout)activity.findViewById(R.id.topMenuBtn0);
        topMenuBtn1 = (FrameLayout)activity.findViewById(R.id.topMenuBtn1);
		topMenuBtn0.setOnClickListener(topBtnListener);
		topMenuBtn1.setOnClickListener(topBtnListener);
		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#663f3833"));
		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon); // bandi logo icon
		icon.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
			}
		});
		
		if(main.videoFullBtn != null) main.videoFullBtn.setVisibility(View.INVISIBLE);		
		progImage = (ImageView) activity.findViewById(R.id.program_image_zone);
		
		if(bandiApp.getIsFMRadio())
		{
			topMenuBtn0.setSelected(true);
			topMenuBtn1.setSelected(false);
		}
		else
		{
			topMenuBtn0.setSelected(false);
			topMenuBtn1.setSelected(true);
		}
		
		progImage.setBackgroundResource(R.drawable.bandi_main_bg2);
		main.repeatListViewInit();
//		http://s-home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=10009938&listType=L&pageSize=5
		main.snsIconCheck();
	}

	
	
	OnClickListener topBtnListener = new OnClickListener()
	{
		
		@Override
		public void onClick(View v) 
		{
			if(v.getId() == R.id.topMenuBtn0)
			{
				if(v.isSelected()) return;
				
				BandiLog.event(context, "OnAir");
				bandiApp.setIsTabClick(true);
				ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
				v.setSelected(true);
				topMenuBtn1.setSelected(false);
				
				bandiApp.setIsFMRadio(true);
				activity.findViewById(R.id.layout_viewRadio).setVisibility(ImageView.VISIBLE);
				
				
				if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
				{
					String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
					Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
//							boardTextLayout.setVisibility(View.GONE);
							new Handler().postDelayed(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
									setting.performClick();
								}
							}, 300);
						}
					}, "설정으로 이동");
					return;
				}
				
				if(serviceConnection.getAudioService() != null)
				{
					serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//					serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
					serviceConnection.getAudioService().switchUrl(AudioService.Channel.FM,
							bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
					serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
				}
				else
				{
					try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
				}

				progImage.setBackgroundResource(R.drawable.bandi_main_bg2);
				
				
				if(main.tabIndex == 0)
				{
					main.tabMenuBtn[0].performClick();
				}
				if(main.tabIndex == 1)
				{
					main.tabMenuBtn[1].performClick();
				}
				else if(main.tabIndex == 2)
				{
					main.tabMenuBtn[2].performClick();
				}
				else if(main.tabIndex == 3)
				{
					main.tabMenuBtn[3].performClick();
				}
				

			}
			else if(v.getId() == R.id.topMenuBtn1)
			{
				if(v.isSelected()) return;
				Log.d(TAG, ">> topMenuBtn1 clicked. ");
				
				BandiLog.event(context, "OnAirIradio");
				bandiApp.setIsTabClick(true);
				ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
				v.setSelected(true);
				
				topMenuBtn0.setSelected(false);
				
				bandiApp.setIsFMRadio(false);
				main.viewRadioHandler.removeCallbacks(main.viewRadioRunnable);
				FrameLayout onAirTools = (FrameLayout)activity.findViewById(R.id.onAirTools);
				onAirTools.setVisibility(View.VISIBLE);
				RelativeLayout controlBar = (RelativeLayout)activity.findViewById(R.id.control_bar);
				controlBar.setVisibility(View.VISIBLE);
//				activity.findViewById(R.id.scroll_text).setVisibility(View.INVISIBLE);
				
				activity.findViewById(R.id.layout_viewRadio).setVisibility(View.INVISIBLE);

				
				if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
				{
					String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
					Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
//							boardTextLayout.setVisibility(View.GONE);
							new Handler().postDelayed(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
									setting.performClick();
								}
							}, 300);
						}
					}, "설정으로 이동");
					return;
				}
					
				if(serviceConnection.getAudioService() != null)
				{
					serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//					serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
					serviceConnection.getAudioService().switchUrl(AudioService.Channel.IRADIO,
							bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
					serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
				}
				else
				{
					try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
				}
				
				
				progImage.setBackgroundResource(R.drawable.bandi_main_bg2);

				if(main.viewRadioCheck.isSelected() && main.tabIndex != -1)
				{
					main.state = 3;
					main.nowScrollHeight = main.subScrollMain.getHeight();
					main.targetScrollHeight = 0;
					
					main.videoFullBtn.setVisibility(View.INVISIBLE);
					main.video.stopPlayback();
					main.videoLayout.setVisibility(View.INVISIBLE);
					main.videoPlayBtn.setVisibility(View.INVISIBLE);
					main.controlLayout.setVisibility(View.VISIBLE);
					main.appImg2.setVisibility(View.INVISIBLE);
					main.writeZone.setVisibility(View.INVISIBLE);
					main.textZone.setVisibility(View.VISIBLE);
					main.viewRadioCheck.setSelected(false);
					main.moveLayout();
					
					new Handler().postDelayed(new Runnable() 
					{
						
						@Override
						public void run() 
						{
							// TODO Auto-generated method stub
							if(main.tabIndex == 0)
							{
								main.tabMenuBtn[0].performClick();
							}
							if(main.tabIndex == 1)
							{
								main.tabMenuBtn[1].performClick();
							}
							else if(main.tabIndex == 2)
							{
								main.tabMenuBtn[2].performClick();
							}
							else if(main.tabIndex == 3)
							{
								main.tabMenuBtn[3].performClick();
							}
						}
					}, 200);
				}
				else if(main.viewRadioCheck.isSelected())
				{
//					main.viewRadioCheck.performClick();
					
					main.state = 3;
					main.nowScrollHeight = main.subScrollMain.getHeight();
					main.targetScrollHeight = 0;
					
					main.videoFullBtn.setVisibility(View.INVISIBLE);
					main.video.stopPlayback();
					main.videoLayout.setVisibility(View.INVISIBLE);
					main.videoPlayBtn.setVisibility(View.INVISIBLE);
					main.controlLayout.setVisibility(View.VISIBLE);
					main.appImg2.setVisibility(View.INVISIBLE);
					main.writeZone.setVisibility(View.INVISIBLE);
					main.textZone.setVisibility(View.VISIBLE);
					main.viewRadioCheck.setSelected(false);
					main.moveLayout();
				}
				else 
				{
					if(main.tabIndex == 0)
					{
						main.tabMenuBtn[0].performClick();
					}
					if(main.tabIndex == 1)
					{
						main.tabMenuBtn[1].performClick();
					}
					else if(main.tabIndex == 2)
					{
						main.tabMenuBtn[2].performClick();
					}
					else if(main.tabIndex == 3)
					{
						main.tabMenuBtn[3].performClick();
					}
				}
				
				
				

			}
		}
		
	};
	
	
	OnClickListener urlBtnListener = new OnClickListener() 
	{
		
		@Override
		public void onClick(View v) 
		{
			Uri uri = null;

			if(v.getId() == R.id.imageButton_home)
			{
				uri = Uri.parse(CProgramData.getInstance().getMobilePageUrl());
			}
			else if(v.getId() == R.id.imageButton_facebook)
			{
				uri = Uri.parse(CProgramData.getInstance().getFacebookUrl());
			}
			else if(v.getId() == R.id.imageButton_kakao)
			{
				uri = Uri.parse(CProgramData.getInstance().getKakaoUrl());
			}
			else if(v.getId() == R.id.imageButton_twitter)
			{
				uri = Uri.parse(CProgramData.getInstance().getTwitterUrl()); 
			}
			
			Intent it  = new Intent(Intent.ACTION_VIEW, uri);
			activity.startActivity(it);
		}
	};


}
