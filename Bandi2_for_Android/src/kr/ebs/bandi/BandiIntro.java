package kr.ebs.bandi;


import java.util.Timer;
import java.util.TimerTask;

import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.data.VersionData;
import kr.ebs.bandi.GoogleAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;


public class BandiIntro extends Activity 
{
	static final String TAG = "BandiIntro";
	
//	int initSettingCount;
//	final int initSettingTotalCount = 4;
	
	Preferences prefs;
	
	BandiApplication bandiApp;
	
	private DisplayMetrics dm;
	private int dpWidth, dpHeight;
	FrameLayout.LayoutParams fParams;
	AnimationDrawable loadingAnimation;
	long startTime;
//	int maxTime = 6000;
	boolean loadingFail = false;
	Timer timer;
	BandiInit bandiInit;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	super.onCreate(savedInstanceState);
        
    	prefs = new Preferences(this);
    	bandiApp = (BandiApplication)getApplication();
    	
//    	// 앱 접근 권한 안내 페이지 노출
//    	if (! prefs.getPermissionAgreement()) {
//    		Intent intent = new Intent(this, PermissionAgreementActivity.class);
//    		startActivity(intent);
//    		finish();
//    		return;
//    	}
    	
    	// os 제공 앱 권한확인? 참고 : https://brunch.co.kr/@babosamo/50
//    	String[] permissions = new String[]{android.Manifest.permission.READ_PHONE_STATE};
//    	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//    		for (String permission : permissions) {
////    			int result = android.support.v4.content.PermissionChecker;
////    			android.support.v4.content.PermissionChecker;
////    			android.support.v4.content.PermissionChecker
//    		}
//    	}
    	
    	
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.intro);
        
        dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
        
//        initSettingCount = 0;
        
        
        /*
         * url 을 통한 실행일 경우, parameter 따라서 채널 설정 (일회성)
         * manifest 파일에서 설정됨
         * @date : 2016-11-08
         */
        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            String channel = uri.getQueryParameter("channel");
            if ("fm".equals(channel)) 
            {
            	bandiApp.setUrlSelectedChannel(AudioService.Channel.FM);
            } 
            else if ("iradio".equals(channel)) 
            {
            	bandiApp.setUrlSelectedChannel(AudioService.Channel.IRADIO);
            }
        }
        
        /*
         * 반디 최초 실행시 HLS , rtsp 스트리밍 사용 판단
         */
        if (! prefs.isSetHLS() 
        		&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)  // sdk_int >= 21 (5.0)
        {
        	prefs.putHLS(true);
        }
        
                
        /*
         * 반디 로딩 이미지 재생.
         */
        fParams = new FrameLayout.LayoutParams(getHeightDP(200), getHeightDP(250), Gravity.CENTER);
        ImageView loading = (ImageView) findViewById(R.id.imageView_loading);
        loading.setLayoutParams(fParams);
        loading.setBackgroundResource(R.drawable.ani_loading);        
        loadingAnimation = (AnimationDrawable) loading.getBackground();
                 
        /*
         * copyright 텍스트 노출.
         */
        fParams = new FrameLayout.LayoutParams(getHeightDP(161), getHeightDP(9), Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        fParams.setMargins(0, 0, 0, getWidthDP(50));
        ImageView licenseImg = (ImageView)findViewById(R.id.imageView_license);
        licenseImg.setLayoutParams(fParams);
        
        /*
         * 가용한 네트워크가 있는지 확인 후, (사용자 설정과 무관) 
         * 없다면, 앱이 시작조차 하지 않음 (2015-06-03 현재) - 로직 수정이 필요할까??
         */
        if(networkCheck() == false)
        {
        	AlertDialog.Builder builder = new AlertDialog.Builder(BandiIntro.this);
		    builder.setMessage("기기에 사용 가능한 네트워크가 확인되지 않습니다.")
		    .setCancelable(false)
		    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
		    {
		    	public void onClick(DialogInterface dialog, int id) 
		    	{
		    		finish();
		    	}
		    }).show();
        }
        else
        {
        	startTime = System.currentTimeMillis();
        	timer = new Timer();
        	timer.schedule(loadingTask, 1000 * 30); // 30초 후에 loadingTask 수행. (인트로 화면에서 최대 대기시간)
        	
//        	Log.d(TAG, "before : " + bandiApp.getStreamUrlAndroid());
        	
        	bandiInit = new BandiInit();
        	bandiInit.start();
        	 
        }
        
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    	
    	//** Google Analytics **
    	new GoogleAnalytics(this).sendScreenView(TAG);
    }
    
        
    /**
     * BandiInitHandler 제어용 - MainActivity 로 전환용
     */
    private final int GOTO_MAIN = 1;
    
    /**
     * BandiInitHandler 제어용 - versionCheck2 에서 업그레이드 다이알로그 노출 필요시 전달 값
     */
    private final int SHOW_UPGRADE_DIALOG = 2;
    
    
    class BandiInitHandler extends Handler
    {
    	@Override
    	public void handleMessage(Message msg)
    	{
    		super.handleMessage(msg);
    		
    		switch(msg.what)
    		{
    		case GOTO_MAIN:
    			if (bandiInit != null && bandiInit.isAlive()) bandiInit.interrupt();
    			
    			Intent intent = new Intent(BandiIntro.this, MainActivity.class);
				startActivity(intent);
				finish();
    			break;
    			
    		case SHOW_UPGRADE_DIALOG:
    			AlertDialog.Builder builder = new AlertDialog.Builder(BandiIntro.this);
			    builder.setMessage("새로운 버전의 반디 프로그램이 출시되었습니다.업데이트 후 이용해주시기 바랍니다.")
			    .setCancelable(false)
			    .setPositiveButton("업데이트", new DialogInterface.OnClickListener() 
			    {
			    	public void onClick(DialogInterface dialog, int id) 
			    	{
			    		dialog.dismiss();
			    		Uri uri = Uri.parse(VersionData.getInstance().getAppURL());
		    			Intent it  = new Intent(Intent.ACTION_VIEW, uri);
		    			startActivity(it);
			    	}
			    })
			    .setNegativeButton("취소", new DialogInterface.OnClickListener() 
			    {
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();
					}
				});
		        AlertDialog alert = builder.create();
		        alert.show();
		        break;
		        
    		default:
//    			Log.d(TAG, "default");
    			break;
    		}
    	}
    }
    
    BandiInitHandler bHandler = new BandiInitHandler();
    
    class BandiInit extends Thread implements Runnable 
    {
    	@Override 
    	public void run()
    	{
    		super.run();
    		long start = System.currentTimeMillis();
    		
    		setStreamingUrl();
        	autoLogin();
        	versionCheck2();
        	eventData2();
        	new TimeTable(BandiIntro.this).retrieveAndSetStartChannel();
        	
        	long workTime = System.currentTimeMillis() - start;
        	
            try {
//				Log.d(TAG, "work time : " + workTime);
				// loadingAnimation.start() 에서 재생되는 시간 고려 테스트한 후 최적화 값
				long playTime = 3600;
            	if (workTime < playTime) Thread.sleep(playTime - workTime); 
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            
            bHandler.sendEmptyMessage(GOTO_MAIN);
    	}
    	
    }
    
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) 
    {
    	super.onWindowFocusChanged(hasFocus);
    	if (hasFocus) 
    	{
            // 어플에 포커스가 갈때 시작된다
    		loadingAnimation.start(); // 총 로딩 시간 3.72초 40frame * 0.09초 + 0.12sec.* 1frame
//    		Log.d(TAG, "animation start");
        } 
    	else 
    	{
            // 어플에 포커스를 떠나면 종료한다
            loadingAnimation.stop();
        }
    }
    
    /**
     * 특정 시간이 지나도 인트로 화면이 유지될 경우 호출됨. <br>
     * MainActivity 로 전환 기능 <br>
     * onCreate() 내에서 Timer 로 예약
     */
    TimerTask loadingTask = new TimerTask() 
    {
		@Override
		public void run() 
		{
			loadingFail = true;
//			loadingErrorPopup();
			
			Intent intent = new Intent(BandiIntro.this, MainActivity.class);
			startActivity(intent);
			finish();
		}
	};
    
	/**
	 * 기기의 가용한 네트워크 존재 여부 반환 <br> 
	 * (앱 내 사용자 설정과 무관) <br>
	 * 
	 * @return 
	 * true: 가용 네트워크 있음 <br> 
	 * false: 가용 네트워크 없음 <br>
	 */
    public boolean networkCheck()
    {
    	boolean isNetwork = true;
    	ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
		if(mobile != null && wifi != null)
		{
			if (mobile.isConnected() || wifi.isConnected())
			{
				isNetwork = true; // WIFI, 3G 어느곳에도 연결되지 않았을때
			}
			else
			{
				isNetwork = false;
			}
		}
		else if(mobile == null && wifi != null)
		{
			if(wifi.isConnected())
			{
				isNetwork = true;
			}
			else
			{
				isNetwork = false;
			}
		}
		else
		{
			isNetwork = false;
		}
		
		
		return isNetwork;
    }
    
    // key 기능 중지..
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) 
    {
        if (keyCode == KeyEvent.KEYCODE_BACK) 
        {
            // pass
        }
        return false;
    }
    
    
    
    @Override
    protected void onDestroy() 
    {
    	if(timer != null) timer.cancel();
    	super.onDestroy();
    }
    
    /**
     * 서버에서 제공하는 반디 업데이트 정보 API 확인 후 처리
     */
    private void versionCheck2() 
    {
//    	Log.d(TAG, "do versionCheck2");
    	
    	try {
    		String jsonStr = Url.getServerText(Url.UPGRADE_CHECK_JSON);
        	JSONObject jObject = new JSONObject(jsonStr);
//			Log.d(TAG, "jObject - " + jObject.toString());
			
			if(jObject != null)
			{
				try
				{
					JSONObject result = jObject.getJSONObject("resultXml");
					
					String resulCode = result.getString("resultCd");
					String appUrl = result.getJSONObject("object").getString("appUrl");
					String appVersion = result.getJSONObject("object").getString("appVer");
					String appMsg = result.getJSONObject("object").getString("appMsg");
					
					VersionData data = VersionData.getInstance();
					data.setAppURL(appUrl);
					data.setAppMessage(appMsg);
					data.setBandiVersion(appVersion);
					
					PackageInfo pi;
					try {
						pi = getPackageManager().getPackageInfo(getPackageName(), 0);
//						Log.d(TAG, "versionCode = " + pi.versionCode);
//						Log.d(TAG, "api version = " + data.getBandiVersion());
						
						if(pi.versionCode < Integer.parseInt(data.getBandiVersion()))
						{
							bandiApp.setNeedToUpgrade(true);
							bHandler.sendEmptyMessage(2);
						}
						else
						{
							bandiApp.setNeedToUpgrade(false);
						}
					} 
					catch (NameNotFoundException e) 
					{
						e.printStackTrace();
					}
				} 
				catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
//			Toast.makeText(this, "Error - 버전 정보 체크", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
    }
    
    
    
    
    /**
     * 디폴트 스트리밍 값을 앱 정보에 할당하고, <br>
     * MainActivity 호출하여 페이지 전환한다.<br>
     * UI thread 에서 실행된다.(runOnUiThread 사용)
     */
    public void loadingErrorPopup()
    {
    	runOnUiThread(new Runnable() 
		{	
			@Override
			public void run() 
			{
				
			    bandiApp.setStreamUrlAndroid(Url.fmAudioStreamRtsp);
				bandiApp.setStreamUrlAndroid2(Url.fmAudioStreamHttp);
				bandiApp.setStreamUrlAndroidVod(Url.fmVideoStreamRtsp);
				bandiApp.setStreamUrlIradioAndroid(Url.iRadioAudioStreamRtsp);
				bandiApp.setStreamUrlIradioAndroid2(Url.iRadioAudioStreamHttp);

//				Log.d(TAG, "go to main in loadingErrorPopup");
				
				Intent intent = new Intent(BandiIntro.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
    }
     
    /**
     * 서버에서 이벤트 정보를 받아온다.
     */
    private void eventData2() 
    {
//    	Log.d(TAG, "do eventData2");
    	 
		try {
			String jsonStr = Url.getServerText(Url.EVENT_DATA_JSON);
			JSONObject jObject = new JSONObject(jsonStr);
//			Log.d(TAG, "jObject - " + jObject.toString());
			
			if(jObject != null)
			{
				JSONArray jArray = jObject.getJSONArray("bandiEventLists");
				for(int i = 0; i < jArray.length(); i++)
				{
					if(jArray.getJSONObject(i).getString("evtClsCd").equals("002"))
					{
						EventData data = new EventData();
						data.setEventID(jArray.getJSONObject(i).getLong("evtId"));
						data.setLinkUrl(jArray.getJSONObject(i).getString("linkUrl"));
						data.setEventTitle(jArray.getJSONObject(i).getString("evtTitle"));
						data.setEventCode(jArray.getJSONObject(i).getString("evtClsCd"));
						data.setEventStartDay(jArray.getJSONObject(i).getString("evtStartDt"));
						data.setEventEndDay(jArray.getJSONObject(i).getString("evtEndDt"));
						data.setEventMessage(jArray.getJSONObject(i).getString("evtCntn"));
						data.setEventUrl(jArray.getJSONObject(i).getString("mobLinkUrl"));
						data.setEventDeviceCode(jArray.getJSONObject(i).getString("shwSiteDsCd"));
						data.setImagePathUrl(jArray.getJSONObject(i).getString("mobThmnlFilePathNm"));
						data.setImagePhsUrl(jArray.getJSONObject(i).getString("mobThmnlFilePhscNm"));
						bandiApp.setEventData(data);
						break;
					}
				}
			}
		} catch (JSONException e) {
//			Log.d(TAG, "event data loading fail");
			e.printStackTrace();
		}
    }
    
    
    
    /**
     * 자동로그인 설정 시 로그인 실행
     * UI Thread 가 아닌 별도 Thread 로 실행해야 함.
     */
    public void autoLogin()
    {
        if(prefs.getAutoLogin())
        {
        	LoginApi login = new LoginApi(this);
//        	Login login = new Login(this);
        	
        	boolean result = login.doAutoLogin();
        	
        	Log.d(TAG, "autoLogin result : " + result);
        }
    }
    
    /**
     * 서버에서 제공하는 API 에서 스트리밍 정보 받아와 사용.
     * 받아오기 실패시 하드코딩된 기본 URL 을 사용한다.
     */
    private void setStreamingUrl() 
    {
    	try 
    	{
    		JSONObject jObject = new JSONObject(Url.getServerText(Url.STREAM_INFO_JSON));
    		
    		JSONArray jArray = jObject.getJSONArray("streamLists");
			
			for(int i = 0; i < jArray.length(); i++)
			{
				if(jArray.getJSONObject(i).getString("svcCd").equals("Bandi"))
				{
					bandiApp.setStreamUrlAndroid(jArray.getJSONObject(i).getString("streamUrlAndroid"));
					bandiApp.setStreamUrlAndroid2(jArray.getJSONObject(i).getString("streamUrlAndroid2"));
					bandiApp.setStreamUrlAndroidVod(jArray.getJSONObject(i).getString("streamUrlAndroidVod"));
					bandiApp.setStreamUrlIradioAndroid(jArray.getJSONObject(i).getString("streamUrlIradioAndroid"));
					bandiApp.setStreamUrlIradioAndroid2(jArray.getJSONObject(i).getString("streamUrlIradioAndroid2"));
					
					bandiApp.setStreamUrlFmHLS(jArray.getJSONObject(i).getString("streamUrlIos"));
					bandiApp.setStreamUrlIRadioHLS(jArray.getJSONObject(i).getString("streamUrlIradioIos"));
				}
			}	
    	} 
    	catch(Exception e) 
    	{
    		bandiApp.setStreamUrlAndroid(Url.fmAudioStreamRtsp);
			bandiApp.setStreamUrlAndroid2(Url.fmAudioStreamHttp);
			bandiApp.setStreamUrlAndroidVod(Url.fmVideoStreamRtsp);
			bandiApp.setStreamUrlIradioAndroid(Url.iRadioAudioStreamRtsp);
			bandiApp.setStreamUrlIradioAndroid2(Url.iRadioAudioStreamHttp);
			
			bandiApp.setStreamUrlFmHLS(Url.fmAudioStreamHLS);
			bandiApp.setStreamUrlIRadioHLS(Url.iRadioAudioStreamHLS);
			
    		e.printStackTrace();
    	} 
    	
    	
    }
   
    
    /**
     * 바탕화면 단축 아이콘 만들기 기능???
     * @param context
     */
    private void addShortcut(Context context) 
    {
    	if(prefs.getShortcut() == false)
    	{
	        Intent shortcutIntent = new Intent();
	        shortcutIntent.setAction(Intent.ACTION_MAIN);
	        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
	        shortcutIntent.setClassName(context, getClass().getName());
	        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        
	        Parcelable iconResource = Intent.ShortcutIconResource.fromContext( this,  R.drawable.ic_bandi_launcher);
	        
	        Intent intent = new Intent();
	        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
	        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
	        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,iconResource);
	        intent.putExtra("duplicate", false);
	        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");       
	        sendBroadcast(intent);
	        prefs.putShortcut(true);
	        
//	        initSettingCount++;
//			if(initSettingCount == initSettingTotalCount)
//			{
//				Intent i = new Intent(BandiIntro.this, MainActivity.class);
//				startActivity(i);
//				finish();
//			}
    	}
    	else
    	{
//    		initSettingCount++;
//			if(initSettingCount == initSettingTotalCount)
//			{
//				Intent i = new Intent(BandiIntro.this, MainActivity.class);
//				startActivity(i);
//				finish();
//			}
    	}
   }  
    
   public int getWidthDP(int dpValue)
   {
	   return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, getResources().getDisplayMetrics());
   }
	
   public int getHeightDP(int dpValue)
   {
	   return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, getResources().getDisplayMetrics());
   }
}

