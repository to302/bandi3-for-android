package kr.ebs.bandi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.GoogleAnalytics;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


public class NotiAndEvent implements BaseInterface
{


	static final String TAG = "NoticeEvent";

	private Context context ;
	private Activity activity;
	private LayoutInflater inflater;
	LinearLayout scrollLayout;
	ScrollView scroll;
	MainActivity main;

	DisplayMetrics dm;
	int dpWidth, dpHeight;

	private FrameLayout.LayoutParams fParams;
	private LinearLayout.LayoutParams lParams;
	private RelativeLayout.LayoutParams rParams;
	
	
	public NotiAndEvent(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;

		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
		
	}

	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.noti_and_event);
		BaseActivity.swapLayout(context, R.layout.layout_noti_and_event);

		setListener();

		// 목록을 불러온다.
		noticeEventListRetriever = new NoticeEventListRetriever();
		noticeEventListRetriever.start();
		
		//** Google Analytics **
    	new GoogleAnalytics(this.activity).sendScreenView(TAG);
	}

	/**
	 * 쓰레드에서 전달하는(서버에서 받아온) 변수 처리
	 */
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg)
		{
			super.handleMessage(msg);

			switch(msg.what)
			{
			case 1:
				makeNoticeEventList((String) msg.obj);
				if (noticeEventListRetriever.isAlive()) noticeEventListRetriever.interrupt();
				break;
			}
		}
	};

	/**
	 * 공지/이벤트 목록을 받아오는 쓰레드 변수 <br>
	 * handler (msg.what = 1)
	 */
	private NoticeEventListRetriever noticeEventListRetriever;
	private class NoticeEventListRetriever extends Thread
	{
		@Override
		public void run()
		{
			String jsonStr = Url.getServerText(Url.EVENT_DATA_JSON);
			Message msg = handler.obtainMessage(1, jsonStr);
			handler.sendMessage(msg);
		}
	}


	/**
	 * 받아온 공지/이벤트 정보로 화면에 목록을 생성.
	 * @param jsonStr - JSON 형식의 String
	 */
	private void makeNoticeEventList(String jsonStr)
	{
		try
		{
			JSONObject jObject = new JSONObject(jsonStr);

			eventData.clear();
			scrollLayout.removeAllViews();
			JSONArray result = jObject.getJSONArray("bandiEventLists");


			for(int i = 0; i < result.length(); i++)
			{
				EventData data = new EventData();
				data.setEventID(result.getJSONObject(i).getLong("evtId"));
				data.setLinkUrl(result.getJSONObject(i).getString("linkUrl"));
				data.setEventTitle(result.getJSONObject(i).getString("evtTitle"));
				data.setEventCode(result.getJSONObject(i).getString("evtClsCd"));
				data.setEventStartDay(result.getJSONObject(i).getString("evtStartDt"));
				data.setEventEndDay(result.getJSONObject(i).getString("evtEndDt"));
				data.setEventMessage(result.getJSONObject(i).getString("evtCntn"));
				data.setEventUrl(result.getJSONObject(i).getString("mobLinkUrl"));
				data.setEventDeviceCode(result.getJSONObject(i).getString("shwSiteDsCd"));
				data.setImagePathUrl(result.getJSONObject(i).getString("mobThmnlFilePathNm"));
				data.setImagePhsUrl(result.getJSONObject(i).getString("mobThmnlFilePhscNm"));

				if(data.getEventCode().equals("002"))
				{
					TimeTable tt = new TimeTable(context);
			        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);

					Date date1 = dateFormat.parse(data.getEventStartDay());
					Date date2 = dateFormat.parse(tt.getDateForXmlRetrieving());

					if(date1.after(date2) == false)
			        {
						eventData.add(data);
			        }
				}
				else
				{
					eventData.add(data);
				}
			}

			View[] view = new View[eventData.size()];
			RelativeLayout[] mainLayout = new RelativeLayout[eventData.size()];
			ImageView[] icon = new ImageView[eventData.size()];
			TextView[] title = new TextView[eventData.size()];
			TextView[] date = new TextView[eventData.size()];
			ImageButton[] detail = new ImageButton[eventData.size()];
			for(int i = 0; i < eventData.size(); i++)
			{
				view[i] = inflater.inflate(R.layout.layout_eventandnoti_list_item, null);

				fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				mainLayout[i] = (RelativeLayout)view[i].findViewById(R.id.layout_eventListItem_main);
				mainLayout[i].setLayoutParams(fParams);
				mainLayout[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
				mainLayout[i].setTag(i);
				mainLayout[i].setOnTouchListener(new OnTouchListener()
				{
					@Override
					public boolean onTouch(View v, MotionEvent event)
					{
						if (event.getAction() == MotionEvent.ACTION_UP)
						{
							main.mainScroll.requestDisallowInterceptTouchEvent(false);
							if(isMove == false)
							{
								try {
									int index = Integer.parseInt(v.getTag().toString());

									if(eventData.get(index).getEventCode().equals("002"))
									{
										TimeTable tt = new TimeTable(context);
								        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);
										Date startDay = dateFormat.parse(eventData.get(index).getEventStartDay());
										Date endDay = dateFormat.parse(eventData.get(index).getEventEndDay());
										Date now = dateFormat.parse(tt.getDateForXmlRetrieving());

										if(now.after(endDay))
										{
											Dialog.showAlert(context, "이벤트 기간이 종료 되었습니다.");
//											AlertDialog.Builder builder = new AlertDialog.Builder(context);
//										    builder.setMessage("이벤트 기간이 종료 되었습니다.")
//										    .setCancelable(false)
//										    .setPositiveButton("확인", new DialogInterface.OnClickListener()
//										    {
//										    	public void onClick(DialogInterface dialog, int id)
//										    	{
//										    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
//										    		dialog.dismiss();
//
//										    	}
//										    });
//											AlertDialog alert = builder.create();
//											alert.show();
										}
										else
										{
											EventDialog d = new EventDialog(context);
											d.setData(eventData.get(index));
											d.show();
										}
									}
									else
									{
										EventDialog d = new EventDialog(context);
										d.setData(eventData.get(index));
										d.show();
									}
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						else if(event.getAction() == MotionEvent.ACTION_DOWN)
						{
							isMove = false;
							startX = event.getRawX();
							startY = event.getRawY();
							main.mainScroll.requestDisallowInterceptTouchEvent(true);
						}
				        else if(event.getAction() == MotionEvent.ACTION_MOVE)
				        {
				        	if(Math.abs(startX - event.getRawX()) > 3 && Math.abs(startY - event.getRawY()) > 3)
				        	{
				        		isMove = true;
				        	}
				        	else
				        	{
				        		isMove = false;
				        	}
				        }

						return true;
					}
				});
				if(i % 2 == 0)
				{
					mainLayout[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
				}
				else
				{
					mainLayout[i].setBackgroundColor(Color.parseColor("#ffffff"));
				}

				lParams = new LinearLayout.LayoutParams(getHeightDP(48), getHeightDP(21));
				icon[i] = (ImageView)view[i].findViewById(R.id.imageView_eventAndNotiIcon);
				icon[i].setLayoutParams(lParams);
				if(eventData.get(i).getEventCode().equals("001"))
				{
					icon[i].setBackgroundResource(R.drawable.ico_notice);
				}
				else if(eventData.get(i).getEventCode().equals("002"))
				{
					icon[i].setBackgroundResource(R.drawable.ico_event);
				}

				lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				lParams.setMargins(getWidthDP(5), 0, getWidthDP(5), 0);
				title[i] = (TextView)view[i].findViewById(R.id.textView_notiEventTitle);
				title[i].setLayoutParams(lParams);
				title[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
				title[i].setText(eventData.get(i).getEventTitle());

				lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				lParams.setMargins(0, getWidthDP(5), 0, 0);
				date[i] = (TextView)view[i].findViewById(R.id.textView_eventDate);
				date[i].setLayoutParams(lParams);
				date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
				if(eventData.get(i).getEventStartDay().equals("null") || eventData.get(i).getEventStartDay() == null)
				{
					date[i].setVisibility(View.GONE);
				}
				else
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy.MM.dd");
					Date sDate = sdf.parse(eventData.get(i).getEventStartDay());
					Date eDate = sdf.parse(eventData.get(i).getEventEndDay());
					date[i].setText("이벤트기간 : " + sdf2.format(sDate) + " ~ " + sdf2.format(eDate));
				}

				rParams = new RelativeLayout.LayoutParams(getHeightDP(29), getHeightDP(29));
				rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				rParams.addRule(RelativeLayout.CENTER_VERTICAL);
				detail[i] = (ImageButton)view[i].findViewById(R.id.imageButton_eventDetail);
				detail[i].setLayoutParams(rParams);
				detail[i].setTag(i);
				detail[i].setOnClickListener(listClickListener);


				scrollLayout.addView(view[i]);

			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // end makeNoticeEventList()


	private ArrayList<EventData> eventData = new ArrayList<EventData>();
	int pageIndex = 0;
	boolean isMove = false;
	float startX, startY;

	@SuppressLint({ "NewApi", "SimpleDateFormat" })
	private void setListener()
	{
		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				activity.findViewById(R.id.layout_menu1).performClick();
			}
		});

		main.repeatListViewInit();

		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));
		scroll = (ScrollView)activity.findViewById(R.id.scrollView_eventAndNoti);

		scrollLayout = (LinearLayout)activity.findViewById(R.id.layout_eventAndNoti);

		// 아래의 기능은 makeNoticeEvent 와 NoticeEventListRetriever 로 분할
//		String url = "http://home.ebs.co.kr/bandi/bandiEventNoticeList";
//    	Map<String, Object> params = new HashMap<String, Object>();
//    	params.put("appDsCd", "02");
//    	params.put("fileType", "json");
//    	AQuery aq = new AQuery(context);
//    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//		{
//    		@Override
//    		public void callback(String url, JSONObject object, AjaxStatus status)
//    		{
//    			try
//    			{
//    				eventData.clear();
//    				scrollLayout.removeAllViews();
//    				JSONArray result = object.getJSONArray("bandiEventLists");
//
//
//					for(int i = 0; i < result.length(); i++)
//					{
//						EventData data = new EventData();
//						data.setEventID(result.getJSONObject(i).getLong("evtId"));
//						data.setLinkUrl(result.getJSONObject(i).getString("linkUrl"));
//						data.setEventTitle(result.getJSONObject(i).getString("evtTitle"));
//						data.setEventCode(result.getJSONObject(i).getString("evtClsCd"));
//						data.setEventStartDay(result.getJSONObject(i).getString("evtStartDt"));
//						data.setEventEndDay(result.getJSONObject(i).getString("evtEndDt"));
//						data.setEventMessage(result.getJSONObject(i).getString("evtCntn"));
//						data.setEventUrl(result.getJSONObject(i).getString("mobLinkUrl"));
//						data.setEventDeviceCode(result.getJSONObject(i).getString("shwSiteDsCd"));
//						data.setImagePathUrl(result.getJSONObject(i).getString("mobThmnlFilePathNm"));
//						data.setImagePhsUrl(result.getJSONObject(i).getString("mobThmnlFilePhscNm"));
//
//						if(data.getEventCode().equals("002"))
//						{
//							TimeTable tt = new TimeTable(context);
//					        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);
//
//							Date date1 = dateFormat.parse(data.getEventStartDay());
//							Date date2 = dateFormat.parse(tt.getDateForXmlRetrieving());
//
//							if(date1.after(date2) == false)
//					        {
//								eventData.add(data);
//					        }
//						}
//						else
//						{
//							eventData.add(data);
//						}
//
//
//					}
//
//					View[] view = new View[eventData.size()];
//					RelativeLayout[] mainLayout = new RelativeLayout[eventData.size()];
//					ImageView[] icon = new ImageView[eventData.size()];
//					TextView[] title = new TextView[eventData.size()];
//					TextView[] date = new TextView[eventData.size()];
//					ImageButton[] detail = new ImageButton[eventData.size()];
//					for(int i = 0; i < eventData.size(); i++)
//					{
//						view[i] = inflater.inflate(R.layout.layout_eventandnoti_list_item, null);
//
//						fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//						mainLayout[i] = (RelativeLayout)view[i].findViewById(R.id.layout_eventListItem_main);
//						mainLayout[i].setLayoutParams(fParams);
//						mainLayout[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
//						mainLayout[i].setTag(i);
//						mainLayout[i].setOnTouchListener(new OnTouchListener()
//						{
//
//							@Override
//							public boolean onTouch(View v, MotionEvent event)
//							{
//								if (event.getAction() == MotionEvent.ACTION_UP)
//								{
//									main.mainScroll.requestDisallowInterceptTouchEvent(false);
//									if(isMove == false)
//									{
//
//										try {
//											int index = Integer.parseInt(v.getTag().toString());
//
//											if(eventData.get(index).getEventCode().equals("002"))
//											{
//												TimeTable tt = new TimeTable(context);
//										        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);
//												Date startDay = dateFormat.parse(eventData.get(index).getEventStartDay());
//												Date endDay = dateFormat.parse(eventData.get(index).getEventEndDay());
//												Date now = dateFormat.parse(tt.getDateForXmlRetrieving());
//
//												if(now.after(endDay))
//												{
//													AlertDialog.Builder builder = new AlertDialog.Builder(context);
//												    builder.setMessage("이벤트 기간이 종료 되었습니다.")
//												    .setCancelable(false)
//												    .setPositiveButton("확인", new DialogInterface.OnClickListener()
//												    {
//												    	public void onClick(DialogInterface dialog, int id)
//												    	{
//												    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
//												    		dialog.dismiss();
//
//												    	}
//												    });
//													AlertDialog alert = builder.create();
//													alert.show();
//												}
//												else
//												{
//													EventDialog d = new EventDialog(context);
//													d.setData(eventData.get(index));
//													d.show();
//												}
//
//											}
//											else
//											{
//												EventDialog d = new EventDialog(context);
//												d.setData(eventData.get(index));
//												d.show();
//											}
//
//										} catch (ParseException e) {
//											// TODO Auto-generated catch block
//											e.printStackTrace();
//										}
//
//									}
//								}
//								else if(event.getAction() == MotionEvent.ACTION_DOWN)
//								{
//									isMove = false;
//									startX = event.getRawX();
//									startY = event.getRawY();
//									main.mainScroll.requestDisallowInterceptTouchEvent(true);
//								}
//						        else if(event.getAction() == MotionEvent.ACTION_MOVE)
//						        {
////						        	isMove = true;
//						        	if(Math.abs(startX - event.getRawX()) > 3 && Math.abs(startY - event.getRawY()) > 3)
//						        	{
//						        		isMove = true;
//						        	}
//						        	else
//						        	{
//						        		isMove = false;
//						        	}
//						        }
//
//								return true;
//							}
//						});
//						if(i % 2 == 0)
//						{
//							mainLayout[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
//						}
//						else
//						{
//							mainLayout[i].setBackgroundColor(Color.parseColor("#ffffff"));
//						}
//
//						lParams = new LinearLayout.LayoutParams(getHeightDP(48), getHeightDP(21));
//						icon[i] = (ImageView)view[i].findViewById(R.id.imageView_eventAndNotiIcon);
//						icon[i].setLayoutParams(lParams);
//						if(eventData.get(i).getEventCode().equals("001"))
//						{
//							icon[i].setBackgroundResource(R.drawable.ico_notice);
//						}
//						else if(eventData.get(i).getEventCode().equals("002"))
//						{
//							icon[i].setBackgroundResource(R.drawable.ico_event);
//						}
//
//						lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//						lParams.setMargins(getWidthDP(5), 0, getWidthDP(5), 0);
//						title[i] = (TextView)view[i].findViewById(R.id.textView_notiEventTitle);
//						title[i].setLayoutParams(lParams);
//						title[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
//						title[i].setText(eventData.get(i).getEventTitle());
//
//						lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//						lParams.setMargins(0, getWidthDP(5), 0, 0);
//						date[i] = (TextView)view[i].findViewById(R.id.textView_eventDate);
//						date[i].setLayoutParams(lParams);
//						date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
//						if(eventData.get(i).getEventStartDay().equals("null") || eventData.get(i).getEventStartDay() == null)
//						{
//							date[i].setVisibility(View.GONE);
//						}
//						else
//						{
//							SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//							SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy.MM.dd");
//							Date sDate = sdf.parse(eventData.get(i).getEventStartDay());
//							Date eDate = sdf.parse(eventData.get(i).getEventEndDay());
//							date[i].setText("이벤트기간 : " + sdf2.format(sDate) + " ~ " + sdf2.format(eDate));
//						}
//
//						rParams = new RelativeLayout.LayoutParams(getHeightDP(29), getHeightDP(29));
//						rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//						rParams.addRule(RelativeLayout.CENTER_VERTICAL);
//						detail[i] = (ImageButton)view[i].findViewById(R.id.imageButton_eventDetail);
//						detail[i].setLayoutParams(rParams);
//						detail[i].setTag(i);
//						detail[i].setOnClickListener(listClickListener);
//
//
//						scrollLayout.addView(view[i]);
//
//					}
//				}
//    			catch (JSONException e)
//    			{
//					e.printStackTrace();
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//    		}
//		});

	}


	OnClickListener listClickListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			try {
				int index = Integer.parseInt(v.getTag().toString());

				if(eventData.get(index).getEventCode().equals("002"))
				{
					TimeTable tt = new TimeTable(context);
			        SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);
					Date startDay = dateFormat.parse(eventData.get(index).getEventStartDay());
					Date endDay = dateFormat.parse(eventData.get(index).getEventEndDay());
					Date now = dateFormat.parse(tt.getDateForXmlRetrieving());

					if(now.after(endDay))
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(context);
					    builder.setMessage("이벤트 기간이 종료 되었습니다.")
					    .setCancelable(false)
					    .setPositiveButton("확인", new DialogInterface.OnClickListener()
					    {
					    	public void onClick(DialogInterface dialog, int id)
					    	{
					    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
					    		dialog.dismiss();

					    	}
					    });
						AlertDialog alert = builder.create();
						alert.show();
					}
					else
					{
						EventDialog d = new EventDialog(context);
						d.setData(eventData.get(index));
						d.show();
					}

				}
				else
				{
					EventDialog d = new EventDialog(context);
					d.setData(eventData.get(index));
					d.show();
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	public int getWidthDP(int dpValue)
	{

		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}

	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}

	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;

//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
}
