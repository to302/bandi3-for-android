/**
 * 보이는 라디오 기능 구현.
 */
package kr.ebs.bandi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

/**
 * @since 2012. 3. 29.
 * @deprecated 사용하지 않는듯..
 */
public class VideoActivity extends Activity {
    private final String TAG = "VideoActivity";
    
    static final String VIDEO_URL = "rtsp://ebsonairandvod.ebs.co.kr/fmradiotablet500k/tablet500k"; // 2014-07-28 변경. ok!
    
//    private Login lg ;
    private Preferences prefs;
    private ImageView ckSaveId;
    private ImageView ckAutoLogin;
    private EditText userId;
    
    BandiApplication bandiApp;
    
    private RelativeLayout writingLayout ;
    /** 게시물 업로드 Runnable Class 변수 */
//    private Runnable poster = new Runnable() 
//    {
//        public void run() 
//        {
//            BandiApplication app = (BandiApplication) getApplicationContext();
//            String charset = "utf-8";// euc-kr
//            try 
//            {
//                TimeTable tt = new TimeTable(VideoActivity.this);
//                String progcd = tt.get().get("PROGCD");
//                EditText eText = (EditText) findViewById(R.id.writing_box);
//                String contents = eText.getText().toString();
//                if ("".equals(contents)) 
//                {
//                    showAlert("내용을 입력해주세요.");
//                    return;
//                }
//                String urlStr = Url.BANDI_BORAD_ACTION + "?addDsCd=02"; // {01:pc, 02:android; 03:iOS, 04:web}
//                //progcd="BK0LNL10000000002";//----------- for test - 인도어.---------------
//                urlStr = urlStr + "&progcd="+ progcd +"&userid="+ app.getUserId();
//                urlStr = urlStr + "&writer="+ URLEncoder.encode(app.getUserName(), charset); 
//                urlStr = urlStr + "&contents=" + URLEncoder.encode(contents, charset);
//                Log.d(TAG, urlStr);
//                URL url = new URL( urlStr );
//                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                
//                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), charset), 8192);
//                String inStr;
//                
//                String resultCode = "";
//                while ((inStr = in.readLine()) != null) 
//                {
//                    if (! "".equals(inStr.trim()) ) resultCode = inStr;
//                    Log.v(TAG, inStr.trim());
//                }
//                
//                in.close();
//                
//                if (! "0".equals(resultCode)) 
//                {
//                    throw new Exception();
//                } 
//                else 
//                {
//                    showWritingLayer(false);
//                    showBoardLayer(true);
//                }
//                
//            } 
//            catch(Exception e) 
//            {
//                Toast.makeText(VideoActivity.this, "Error - " + e.getMessage(), Toast.LENGTH_SHORT).show();
//                e.printStackTrace();
//            }
//        }
//    };
    
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        
//        this.lg = new Login(this);
        this.prefs = new Preferences(this);
        this.userId = (EditText) findViewById(R.id.userid);
        this.ckSaveId = (ImageView) findViewById(R.id.btn_save_id);
        this.ckAutoLogin = (ImageView) findViewById(R.id.btn_auto_login);
        
        bandiApp = (BandiApplication)getApplication();
        
//        setListener();
        startVideo();
        
        /*
		 * 접근 로그 저장
		 */
		BandiLog.event(this, TAG);
    }
    
    
    private void startVideo()
    {
        VideoView video = (VideoView) findViewById(R.id.video);
        Uri uri = Uri.parse(bandiApp.getStreamUrlAndroidVod()); 
        video.setVideoURI(uri);
        video.start();
        video.setOnPreparedListener(new OnPreparedListener() 
        {
        	// 백그라운드에 로딩 이미지 적용할까 했는데.. 잘 안되서.. 일단 보류.	
			public void onPrepared(MediaPlayer mp) 
			{
				//VideoView video = (VideoView) findViewById(R.id.video);
				//video.setBackgroundResource(0);
			}
        	
        });
        
    }
    
    private void setListener() 
    {
        // 보이는 라디오 화면.
//        ImageView btnHome = (ImageView) findViewById(R.id.view_home);
//        btnHome.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//            	final AudioService audioService = AudioServiceConnection.getInstance().getAudioService();
//				audioService.initPlayer();
//                
//				finish();
//            }
//        });
//        
//        ImageView btnWrite = (ImageView) findViewById(R.id.view_write);
//        btnWrite.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//                showVideoLayer(false);
//                
//                boolean needToLogin = true;
//                BandiApplication app = (BandiApplication) getApplicationContext();
//                if ("".equals(app.getUserId())) 
//                {
//                    if (prefs.getAutoLogin()) 
//                    {
//                        if (!("".equals(prefs.getUserId()) || "".equals(prefs.getPassword()))) 
//                        {
//                            if (lg.login(prefs.getUserId(), prefs.getPassword()) ) 
//                            {
//                                needToLogin = false;
//                            }
//                        }
//                    }
//                    if (needToLogin) showLoginLayer(true);
//                    else showWritingLayer(true);
//                }
//                else
//                {
//                    showWritingLayer(true);
//                }
//            }
//        });
//        
//        ImageView btnBoard = (ImageView) findViewById(R.id.view_board);
//        btnBoard.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//                showVideoLayer(false);
//                showBoardLayer(true);
//            }
//        });
//        
//        // 게시판 보기 화면
//        ImageView btnExit = (ImageView) findViewById(R.id.exit_board);
//        btnExit.setOnClickListener(new OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                showBoardLayer(false);
//                showVideoLayer(true);
//            }
//        });
//        
//        ImageView btnReload = (ImageView) findViewById(R.id.reload_board);
//        btnReload.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//                reloadBoard();
//            }
//        });
//        
//        // 로그인 화면 
//        ImageView btnLogin = (ImageView) findViewById(R.id.btn_login);
//        btnLogin.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//                
//                if ("".equals(getUserId()) || "".equals(getPassword()) ) 
//                {
//                    showAlert("아이디와 패스워드를 모두 입력하세요.");
//                    return;
//                }
//                
//                if (lg.login(getUserId(), getPassword())) 
//                {
//                    savePreferences();
//                    showLoginLayer(false);
//                    showWritingLayer(true);
//                }
//            }
//        });
//        
//        ImageView btnLoginExit = (ImageView) findViewById(R.id.btn_login_exit);
//        btnLoginExit.setOnClickListener(new OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                showLoginLayer(false);
//                showVideoLayer(true);
//            }
//        });
//        
//        // 아이디 저장 버튼이 있으면 저장 아이디 보이기.
//        if (prefs.getSaveId() && !"".equals(prefs.getUserId()))
//        {
//            ckSaveId.setSelected(true);
//            userId.setText(prefs.getUserId());
//        }
//        
//        // 게시물 작성 화면
//        ImageView btnSend = (ImageView) findViewById(R.id.btn_send);
//        btnSend.setOnClickListener(new OnClickListener() 
//        {
//            public void onClick(View v) 
//            {
//                postArticle();
//            }
//        });
//        
//        ImageView btnClose = (ImageView) findViewById(R.id.btn_close);
//        btnClose.setOnClickListener(new OnClickListener()
//        {
//            public void onClick(View v) 
//            {
//                showWritingLayer(false);
//                showVideoLayer(true);
//            }
//        });
        
        
    }
    
    @Override
    protected void onPause()
    {
    	
    	super.onPause();
    	
    	if(prefs.getBackgroundRun() == false)
    	{
    		finish();
    	}
    }
    
    @Override
    public void onBackPressed() 
    {
    
    	super.onBackPressed();
    	
    	finishActivity(1001);
    }
    
//    /** 보이는 라디오 layer 보이기/ 가리기 . */
//    private void showVideoLayer(boolean show) {
//        FrameLayout layout = (FrameLayout) findViewById(R.id.video_base);
//        VideoView video = (VideoView) findViewById(R.id.video);
//        try {
//        	if (show) {
//        		layout.setVisibility(View.VISIBLE);
//        	}
//        	else {
//        		layout.setVisibility(View.GONE); // invisible -> gone 으로 수정 (비디오가 그대로 보이는 현상 개선)
//        	}
//        } catch(Exception e) {
//        	Log.d(TAG, "error - showVideoLayer");
//        }
//    }
//    
//    /** 로그인 layer 보이기/가리기. */
//    private void showLoginLayer(boolean show) {
//        RelativeLayout layout = (RelativeLayout) findViewById(R.id.login_form_wide);
//        if (show) {
//        	layout.setVisibility(View.VISIBLE);
//        }
//        else {
//        	layout.setVisibility(View.INVISIBLE);
//        }
//    }
//    
//    /**
//     * 게시물 등록.
//     * @since 2012. 3. 30.
//     */
//    private void postArticle() {
//        Handler handler = new Handler();
//        handler.post(poster);
//    }
//    
//    /**
//     * 게시물 작성 layer 보이기/가리기.
//     * @since 2012. 3. 30.
//     */
//    private void showWritingLayer(boolean show) {
//        writingLayout = (RelativeLayout) findViewById(R.id.writing_layout);
//        if (show) writingLayout.setVisibility(View.VISIBLE);
//        else writingLayout.setVisibility(View.INVISIBLE);
//        
//        BandiLog.event(this, "BandiBoardWrite_V");
//    }
//    
//    
//    private void showBoardLayer(boolean show) {
//        FrameLayout layout = (FrameLayout) findViewById(R.id.bandi_board_wide);
//        
//        if (show) {
//            layout.setVisibility(View.VISIBLE);
//            
//            WebView wview = (WebView) findViewById(R.id.webview_wide);
//            wview.getSettings().setJavaScriptEnabled(true);
//            wview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//            
//            TimeTable tt = new TimeTable(this);
//            HashMap<String, String> tmap = tt.get();
//            
//            wview.loadUrl(Url.BANDI_BOARD + tmap.get("PROGCD"));
//            wview.setWebViewClient(new BoardWebViewClient());
//        } else {
//            layout.setVisibility(View.INVISIBLE);
//        }
//        
//        BandiLog.event(this, "BandiBoard_V");
//    }
//    
//    
//    private void reloadBoard() {
//        WebView wview = (WebView) findViewById(R.id.webview_wide);
//        wview.reload();
//    }
//    
//    private class BoardWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//        
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            super.onPageStarted(view, url, favicon);
//        }
//        
//        @Override  
//        public void onPageFinished(WebView view, String url) {  
//            super.onPageFinished(view, url);
//        }  
//    }
//    
//    private void showAlert(String msg) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(msg)
//               .setCancelable(false)
//               .setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                   public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                   }
//               });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }
//    
//    /**
//     * 사용자 설정 정보 저장.
//     * @since 2012. 3. 9.
//     */
//    private void savePreferences() {
//        prefs.putSaveId( this.ckSaveId.isSelected() );
//        prefs.putAutoLogin( this.ckAutoLogin.isSelected() );
//        
//        if (this.ckAutoLogin.isSelected()) {
//            prefs.putUserId(getUserId());
//            prefs.putPassword(getPassword());
//        } else {
//            prefs.putUserId("");
//            prefs.putPassword("");
//            
//            if (this.ckSaveId.isSelected()) prefs.putUserId(getUserId());
//        }
//    }
//    
//    private String getUserId() {
//        return userId.getText().toString();
//    }
//    
//    private String getPassword() {
//        EditText passwd = (EditText) findViewById(R.id.passwd);
//        return passwd.getText().toString();
//    }
//    
//    @Override
//    public void onBackPressed () {
//    	findViewById(R.id.view_home).performClick();
//    }
    
}
