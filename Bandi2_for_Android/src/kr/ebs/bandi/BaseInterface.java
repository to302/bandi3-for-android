package kr.ebs.bandi;


public interface BaseInterface 
{
	
	/**
	 * 메뉴가 바뀌면 수행하는 메소드
	 */
	public void run();
}
