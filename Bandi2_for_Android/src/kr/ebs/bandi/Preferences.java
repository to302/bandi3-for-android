/**
 * ����� ���� ���� ó��.
 */
package kr.ebs.bandi;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Preferences {
	static final String TAG = "Preferences";
	
    static final String PREFS_NAME = "preferences";
    
    private Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    
    private final String ENCRYPTED = "encrypted"; // 암호화 저장 사용 여부
    private final String USER_ID = "user_id";
    private final String PASSWORD = "password";
    private final String SAVE_ID = "save_id"; // ID 저장 기능 사용여부 
    private final String AUTO_LOGIN = "auto_login"; // 자동 로그인 기능 사용여부
    private final String SECURE_LOGIN = "secure_login"; // 보안로그인 (use https + post)
    private final String USE_HLS = "secure_login"; // HLS 스트리밍 사용
    
    private final String USE_PAID_NETWORK = "use_paid_network";
    private final String AUTO_RUN = "auto_run";
    private final String WAKE_TIME = "wake_time";
    private final String FINISH_TIME = "finish_time";
    private final String AUTO_FINISH_STATE = "auto_close_state";
    private final String CLOSE_UNTIL = "close_until";
    
    private final String BROAD_LIST = "broad_list";
    private final String PROGRAM_DATA = "program_data";
    private final String START_CHANNEL = "start_channel";
    private final String BACKGROUND_RUN = "background_running";
    
    private final String SHORT_CUT = "short_cut";
    private final String TUTORIAL = "tutorial";
    
    /**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    private final String PERM_AGREE = "permission_agreement";
    
    public Preferences(Context context)
    {
        this.context = context;
    	prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }
    
    /**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    public void putPermissionAgreement(boolean isAgreed)
    {
    	editor = prefs.edit();
    	editor.putBoolean(PERM_AGREE, isAgreed);
    	editor.commit();
    }
    
    /**
     * 앱 필수 접근권한 동의 체크여부 저장
     */
    public boolean getPermissionAgreement()
    {
    	return prefs.getBoolean(PERM_AGREE, false);
    }
    
    public void putShortcut(boolean isShortcut)
    {
    	editor = prefs.edit();
    	editor.putBoolean(SHORT_CUT, isShortcut);
    	editor.commit();
    }
    
    public boolean getShortcut()
    {
    	return prefs.getBoolean(SHORT_CUT, false);
    }
    
    public void putTutorialState(boolean isView)
    {
    	editor = prefs.edit();
    	editor.putBoolean(TUTORIAL, isView);
    	editor.commit();
    }
    
    public boolean getTutorialState()
    {
    	return prefs.getBoolean(TUTORIAL, false);
    }
    
    public void putBackgroundRun(boolean isRun)
    {
    	editor = prefs.edit();
    	editor.putBoolean(BACKGROUND_RUN, isRun);
    	editor.commit();
    }
    
    public boolean getBackgroundRun()
    {
    	return prefs.getBoolean(BACKGROUND_RUN, true);
    }
    
    
    public void putIsStartFMRadio(boolean isRadioStart)
    {
    	editor = prefs.edit();
    	editor.putBoolean(START_CHANNEL, isRadioStart);
    	editor.commit();
    }
    
    /**
     * 시작채널 설정값을 반환한다.<br />
     * URL을 통한 반디 실행 시, 설정 메뉴가 아닐 경우 URL을 통해 설정된 메뉴값이 반환된다. <br />
     * URL 호출 예) bandi://launch?channel=fm (or iradio) 
     * @return
     */
    public boolean getIsStartFMRadio()
    {
    	/*
    	 * URL을 통한 앱 구동 시, parameter 로 받은 시작 채널 반환
    	 * 설정 메뉴에서는 URL 로 받은 값이 아닌 실제 저장된 값을 반환해야 한다.(*)
    	 * @date : 2016-11-09
    	 */
    	StackTraceElement[] stacks = new Throwable().getStackTrace();
    	String beforeClassName = "";
    	if (stacks.length > 1) beforeClassName = stacks[1].getClassName();
//    	Log.d(TAG, "beforeClassName : " + beforeClassName);

    	if (! beforeClassName.endsWith("Setting"))	
    	{
	    	BandiApplication bandiApp = (BandiApplication) context.getApplicationContext();
	    	AudioService.Channel channel = bandiApp.getUrlSelectedChannel();
//	    	Log.d(TAG, "url channel is : " + channel );
	    	if (channel != null && !"".equals(channel)) {
//	    		Log.d(TAG, "return value : " + (channel == AudioService.Channel.FM));
	    		return (channel == AudioService.Channel.FM); 
	    	}	
    	}
    	
    	return prefs.getBoolean(START_CHANNEL, true);
    }
    
    
    public boolean isEncrypted() 
    {
    	return prefs.getBoolean(ENCRYPTED, false);
    }
    
    public void putUserId(String userId)
    {
        editor = prefs.edit();
        if (! "".equals(userId.trim())) {
        	editor.putString(USER_ID, AES256Cipher.encryptString(userId, context));
        	editor.putBoolean(ENCRYPTED, true);
        }
        editor.commit();
    }
    
    public String getUserId()
    {
    	if (isEncrypted()) {
    		String userId = prefs.getString(USER_ID, "");
    		
    		if (! "".equals(userId.trim())) {
    			return AES256Cipher.decryptString(userId, context);
    		}
    	}
    	return prefs.getString(USER_ID, "");
    }
    
    
    public void putPassword(String password) 
    {
        editor = prefs.edit();
        if (! "".equals(password.trim())) {
        	editor.putString(PASSWORD, AES256Cipher.encryptString(password, context));
        	editor.putBoolean(ENCRYPTED, true);
        }
        editor.commit();
    }
    
    public String getPassword()
    {
    	if (isEncrypted()) {
    		String password = prefs.getString(PASSWORD, "");
    		if (! "".equals(password.trim())) return AES256Cipher.decryptString(password, context);	
    	}
    	
        return prefs.getString(PASSWORD, "");
    }
    
    
    public void putSaveId(boolean flag)
    {
        editor = prefs.edit();
        editor.putBoolean(SAVE_ID, flag);
        editor.commit();
    }
    
    public boolean getSaveId()
    {
        return prefs.getBoolean(SAVE_ID, false);
    }
    
    public void putAutoLogin(boolean flag) 
    {
        editor = prefs.edit();
        editor.putBoolean(AUTO_LOGIN, flag);
        editor.commit();
    }
    
    public boolean getAutoLogin() 
    {
        return prefs.getBoolean(AUTO_LOGIN, false);
    }
    
    public void putSecureLogin(boolean flag)
    {
    	editor = prefs.edit();
        editor.putBoolean(SECURE_LOGIN, flag);
        editor.commit();
    }
    
    public boolean getSecureLogin() 
    {
        return prefs.getBoolean(SECURE_LOGIN, true);
    }
    
    /**
     * HLS 스트리밍 설정 저장
     * @param flag
     */
    public void putHLS(boolean flag)
    {
    	editor = prefs.edit();
        editor.putBoolean(USE_HLS, flag);
        editor.commit();
    }
    
    /**
     * HLS 스트리밍 설정 반환
     * @param flag
     */
    public boolean getHLS() 
    {
        return prefs.getBoolean(USE_HLS, false);
    }
    
    /**
     * HLS 사용 설정값이 지정되었나 확인
     * @return
     */
    public boolean isSetHLS() 
    {
    	return prefs.contains(USE_HLS);
    }
    
    
    public void putAutoFinishState(boolean state)
    {
    	editor = prefs.edit();
    	editor.putBoolean(AUTO_FINISH_STATE, state);
    	editor.commit();
    }
    
    public boolean getAutoFinishState()
    {
    	return prefs.getBoolean(AUTO_FINISH_STATE, false);
    }
    
    /*
     * 3G/LTE/Wibro 등 유료 네트워크 이용 여부 체크
     */
    public void putUsePaidNetwork(boolean flag) 
    {
        editor = prefs.edit();
        editor.putBoolean(USE_PAID_NETWORK, flag);
        editor.commit();
    }
    
    public boolean getUsePaidNetwork() 
    {
        return prefs.getBoolean(USE_PAID_NETWORK, false);
    }
    
    
    
    /*
     * 자동실행 수행 여부 체크
     */
    public void putAutoRun(boolean flag)
    {
        editor = prefs.edit();
        editor.putBoolean(AUTO_RUN, flag);
        editor.commit();
    }
    
    public boolean getAutoRun() 
    {
        return prefs.getBoolean(AUTO_RUN, false);
    }
    
    
    /*
     * 요일별 자동 실행 요일 설정.
     */
    public void putDayOfWeek(Integer dayOfWeek, boolean flag) 
    {
    	editor = prefs.edit();
    	editor.putBoolean("DAY_OF_WEEK_"+ String.valueOf(dayOfWeek), flag);
    	editor.commit();
    }
    
    public boolean getDayOfWeek(Integer dayOfWeek) 
    {
    	return prefs.getBoolean("DAY_OF_WEEK_"+ String.valueOf(dayOfWeek), false);
    }
    
    
    public void putWakeTime(String time) 
    {
        editor = prefs.edit();
        editor.putString(WAKE_TIME, time);
        editor.commit();
    }
    
    public String getWakeTime() 
    {
        return prefs.getString(WAKE_TIME, "");
    }
    
    public void putFinishTime(String time)
    {
    	editor = prefs.edit();
        editor.putString(FINISH_TIME, time);
        editor.commit();
    }
    
    public String getFinishTime()
    {
    	return prefs.getString(FINISH_TIME, "00:00");
    }
    
    // -- 팝업 배너 하루 열지 않기 기능
    public void putBannerClosingTime(int hours) 
    {
    	Date date = new Date();
    	editor = prefs.edit();
        editor.putLong(CLOSE_UNTIL, date.getTime() + 1000*60*60*hours);
        editor.commit();
    }
    
    public boolean inBannerClosedTime() 
    {
    	long thisTime = new Date().getTime();
    	return (thisTime < prefs.getLong(CLOSE_UNTIL, 0)); // �����Ǿ����� ������ ����(�����ð��� �ƴ�:false)���� ����
    }
    
    public void putBoardList(String list)
    {
    	editor = prefs.edit();
    	if(prefs.contains(BROAD_LIST))
    	{
    		editor.remove(BROAD_LIST);
    	}
    	
    	editor.putString(BROAD_LIST, list);
    	editor.commit();
    }
    
    public void putProgramData(String data)
    {
    	editor = prefs.edit();
    	
    	editor.putString(PROGRAM_DATA, data);
    	editor.commit();
    }
    
    public void removeProgramData()
    {
    	editor = prefs.edit();
    	editor.remove(PROGRAM_DATA);
    	editor.commit();
    }
    
    public String getProgramData()
    {
    	return prefs.getString(PROGRAM_DATA, "null");
    }
}
