package kr.ebs.bandi;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.AlarmManager.AlarmClockInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoRunReceiver2 extends BroadcastReceiver {
	static final String TAG = "AutoRunReceiver2";
	
	private Preferences prefs = null;
	private Context context = null;
	
	public AutoRunReceiver2() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		this.prefs = new Preferences(context);
		
		reserveAutoRun();
		
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}
	
	/**
	 * 자동 실행 설정을 확인하고 추가 재설정한다.
	 */
	private void reserveAutoRun() {
		
		if (! this.prefs.getAutoRun()) {
			return;
		}
		
		AlarmManager am = (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);
		Calendar krNow = DateUtil.getKoreanCalendar();
		int[] wakeTime = DateUtil.parseTimeString(this.prefs.getWakeTime());
		Intent intentMain = new Intent(context, MainActivity.class);
		Intent intentReceiver = new Intent(context, AutoRunReceiver2.class);
		
		int[] dayOfWeek = {Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};
		for (int dow : dayOfWeek) {
			if (this.prefs.getDayOfWeek(dow)) {
				PendingIntent pi = PendingIntent.getActivity(context, dow, intentMain, 0);
				int diff = dow - krNow.get(Calendar.DAY_OF_WEEK);
				
				Calendar setCal = DateUtil.getKoreanCalendar(); // 예약된 날짜시간
        		setCal.set(Calendar.HOUR_OF_DAY, wakeTime[0]);
        		setCal.set(Calendar.MINUTE, wakeTime[1]);
        		setCal.set(Calendar.SECOND, 0);
        		setCal.add(Calendar.DAY_OF_MONTH, diff);
        		
        		if (setCal.before(krNow)) setCal.add(Calendar.DAY_OF_MONTH, 7);
        		
        		PendingIntent pi2=PendingIntent.getActivity(context, setCal.get(Calendar.DAY_OF_WEEK), intentMain, 0);
				AlarmClockInfo acInfo = new AlarmClockInfo(setCal.getTimeInMillis(), pi2);
				
				PendingIntent pi3=PendingIntent.getBroadcast(context, setCal.get(Calendar.DAY_OF_WEEK), intentReceiver, 0);
				am.setAlarmClock(acInfo, pi3);
			}
		}
		
		//** 알람 설정 검정용 확인 로그 **
		AlarmClockInfo ac1 = am.getNextAlarmClock();
		if (ac1 != null) {
			Calendar krCal = DateUtil.getKoreanCalendar();
			krCal.setTimeInMillis(ac1.getTriggerTime());
			Log.d(TAG, "NextAlarmClock : " + DateUtil.getDateString(krCal, "yyyy-MM-dd HH:mm"));
		}
		//** 알람 설정 검정용 확인 로그 **/
		
		
	}
}
