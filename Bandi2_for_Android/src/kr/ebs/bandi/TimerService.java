package kr.ebs.bandi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.facebook.Session;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class TimerService extends Service 
{
	final static String TAG = "TimerService";
	
	private Handler handler = new Handler();
	private String finishTime = "";
	private Preferences prefs;
	
	public TimerService() 
	{
		Log.d(TAG, "TimerService()");
	}
	
	private final IBinder serviceBinder = new ServiceBinder();
    public class ServiceBinder extends Binder
    {
    	TimerService getService() 
    	{
            return TimerService.this;
        }
    }
    
    Runnable countdown = new Runnable() 
    {
		@Override
		public void run()
		{
			
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			Log.d(TAG, "getNowDateTime() end");
			String nowTime = new SimpleDateFormat("HH:mm").format(date);

			if(nowTime.equals(finishTime))
			{
				Session session = Session.getActiveSession();
                if(session != null) session.closeAndClearTokenInformation();
				
                prefs.putAutoFinishState(false);
				sendBroadcast(new Intent("quit_bandi"));
				stopSelf();
			}
			else
			{
				handler.postAtTime(this, SystemClock.uptimeMillis() + (1 * 1000)); // 1�� �� ����.
			}
			
		}
    };
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {    	
    	prefs = new Preferences(this);
    	if(intent != null) finishTime = intent.getStringExtra("finishTime");
    	else finishTime = "00:00";
        handler.removeCallbacks(countdown);
        handler.postAtTime(countdown, SystemClock.uptimeMillis() + (1 * 1000)); // 1�� �� ����.
        return START_STICKY;
    }
    
    @Override
	public void onDestroy() 
    {
		Log.d(TAG, "onDestroy()");
		handler.removeCallbacks(countdown);
	}
    
	@Override
	public IBinder onBind(Intent intent)
	{
		Log.d(TAG, "onBind()");
		Log.d(TAG, Boolean.toString(serviceBinder instanceof ServiceBinder));
		return serviceBinder;
	}
	
	@Override
	public void onRebind(Intent intent) 
	{
        Log.d(TAG, "onRebind()");
    }
	
	@Override
	public boolean onUnbind(Intent intent) 
	{
		Log.d(TAG, "onUnbind()");
        return true;
    }
	
	public String getFinishTime() 
	{
//		Log.d(TAG, "remainingSeconds : " + remainingSeconds);
		return finishTime;
	}
	
}
